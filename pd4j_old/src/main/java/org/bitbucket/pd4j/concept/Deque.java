
package org.bitbucket.pd4j.concept;

/**
 *
 * @author pataky
 */
public interface Deque<T, ImplT extends Deque<T, ImplT>> extends Queue<T, ImplT>, Seq<T, ImplT> {
    T last();
    ImplT init();
}
