/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.pd4j.concept;

import org.bitbucket.pd4j.util.Option;

/**
 *
 * @author pataky
 */
public interface OrderedMap<K, V, Ord, Impl extends OrderedMap<K, V, Ord, Impl>> {
    Impl empty();
    Option<V> get(K key);
    Impl put(K key, V value);
    Impl remove(K key);
    boolean isEmpty();
}
