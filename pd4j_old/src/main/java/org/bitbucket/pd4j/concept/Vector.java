/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.pd4j.concept;

/**
 *
 * @author pataky
 */
public interface Vector<T, ImplT extends Vector<T, ImplT>> {
    ImplT empty();
    T get(int i);
    ImplT resize(int newSize);
    ImplT set(int i, T t);
    int size();
    boolean isEmpty();
}
