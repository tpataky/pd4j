/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.pd4j.concept;

/**
 *
 * @author pataky
 */
public interface OrderedSet<T, Ord, Impl extends OrderedSet<T, Ord, Impl>> {
    Impl empty();
    boolean contains(T elem);
    Impl add(T elem);
    Impl remove(T elem);
    boolean isEmpty();
}
