/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.pd4j.concept;

import org.bitbucket.pd4j.util.Option;

/**
 *
 * @author pataky
 */
public interface HashMap<K, V, Impl extends HashMap<K, V, Impl>> {
    Impl empty();
    Option<V> get(K key);
    Impl put(K key, V value);
    Impl remove(K key);
}
