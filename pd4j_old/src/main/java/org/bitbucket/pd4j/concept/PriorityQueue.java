/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.pd4j.concept;

import java.util.Comparator;

/**
 *
 * @author pataky
 */
public interface PriorityQueue<T, Ord extends Comparator<T>, Impl extends PriorityQueue<T, Ord, Impl>> {
    Impl empty();
    T peek();
    Impl pop();
    Impl push(T t);
    boolean isEmpty();
}
