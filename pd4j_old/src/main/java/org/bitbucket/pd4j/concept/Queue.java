/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.pd4j.concept;

/**
 *
 * @author pataky
 */
public interface Queue<T, ImplT extends Queue<T, ImplT>> {
    ImplT empty();
    ImplT snoc(T t);
    T head();
    ImplT tail();
    boolean isEmpty();
}
