/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.pd4j.concept;

/**
 *
 * @author pataky
 */
public interface Seq<T, ImplT extends Seq<T, ImplT>> {
    ImplT empty();
    ImplT cons(T t);
    T head();
    ImplT tail();
    boolean isEmpty();
}
