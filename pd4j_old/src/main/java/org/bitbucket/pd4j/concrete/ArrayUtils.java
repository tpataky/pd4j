/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.pd4j.concrete;

import java.util.Arrays;

/**
 *
 * @author pataky
 */
class ArrayUtils {

    static Object[] array(Object... args) {
        return args;
    }

    static Object[] appL(Object o, Object[] arr) {
        Object[] toReturn = new Object[arr.length + 1];
        toReturn[0] = o;
        System.arraycopy(arr, 0, toReturn, 1, arr.length);
        return toReturn;
    }

    static Object[] popL(Object[] arr) {
        return Arrays.copyOfRange(arr, 1, arr.length);
    }

    static Object[] appR(Object[] arr, Object o) {
        Object[] toReturn = new Object[arr.length + 1];
        toReturn[arr.length] = o;
        System.arraycopy(arr, 0, toReturn, 0, arr.length);
        return toReturn;
    }

    static Object[] popR(Object[] arr) {
        return Arrays.copyOfRange(arr, 0, arr.length - 1);
    }

    static Object[] remove(Object[] arr, int i) {
        Object[] toReturn = new Object[arr.length - 1];
        System.arraycopy(arr, 0, toReturn, 0, i);
        System.arraycopy(arr, i + 1, toReturn, i, arr.length - i - 1);
        return toReturn;
    }

    static Object[] take(Object[] arr, int i) {
        Object[] toReturn = new Object[i];
        System.arraycopy(arr, 0, toReturn, 0, i);
        return toReturn;
    }

    static Object[] drop(Object[] arr, int i) {
        Object[] toReturn = new Object[arr.length - i];
        System.arraycopy(arr, i + 1, toReturn, 0, arr.length - i - 1);
        return toReturn;
    }

    static Object[] app(Object[] a, Object[] b) {
        Object[] toReturn = new Object[a.length + b.length];
        System.arraycopy(a, 0, toReturn, 0, a.length);
        System.arraycopy(b, 0, toReturn, a.length, b.length);
        return toReturn;
    }
}
