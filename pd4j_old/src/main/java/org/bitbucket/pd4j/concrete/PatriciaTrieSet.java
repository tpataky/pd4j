/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.pd4j.concrete;

import java.util.Iterator;
import org.bitbucket.pd4j.concept.HashSet;
import org.bitbucket.pd4j.concept.Seq;

import java.util.Arrays;
import org.bitbucket.pd4j.util.Memoize;

/**
 *
 * @author pataky
 */
public abstract class PatriciaTrieSet<K> implements HashSet<K, PatriciaTrieSet<K>>, Iterable<K> {

    private static Empty emptyInstance = new Empty();

    public static <K> PatriciaTrieSet<K> emptySet() {
        return emptyInstance;
    }

    public PatriciaTrieSet<K> empty() {
        return emptyInstance;
    }

    public boolean contains(K key) {
        return contains(key.hashCode(), key);
    }

    protected abstract boolean contains(int hash, K key);

    public PatriciaTrieSet<K> add(K key) {
        return add(key.hashCode(), key);
    }

    protected abstract PatriciaTrieSet<K> add(int hash, K key);

    public final Seq<K, ?> entrySeq() {
        return getEntrySeq();
    }

    protected abstract LazyList<K> getEntrySeq();

    public Iterator<K> iterator() {
        return new SeqIterator<K>(getEntrySeq());
    }

    static class Empty<K> extends PatriciaTrieSet<K> {

        @Override
        protected PatriciaTrieSet<K> add(int hash, K key) {
            return new Bucket<K>(hash, key);
        }

        public boolean contains(int hash, K key) {
            return false;
        }

        public PatriciaTrieSet<K> remove(K key) {
            return this;
        }

        @Override
        protected LazyList<K> getEntrySeq() {
            return LazyList.emptyStream();
        }

    }

    static class Node<K> extends PatriciaTrieSet<K> {
        private final int bits;
        private final int depth;
        private final PatriciaTrieSet<K> left;
        private final PatriciaTrieSet<K> right;

        public Node(int bits, int depth, PatriciaTrieSet<K> left, PatriciaTrieSet<K> right) {
            this.bits = bits;
            this.depth = depth;
            this.left = left;
            this.right = right;
        }

        @Override
        protected PatriciaTrieSet<K> add(int hash, K key) {
            int position = 1;
            for (int i = 0; i < depth; i++) {
                int hp = hash & position;
                int bp = bits & position;
                if (hp != bp) {
                    if (hp == 0) {
                        return new Node<K>(bits, i, new Bucket<K>(hash, key), this);
                    } else {
                        return new Node<K>(bits, i, this, new Bucket<K>(hash, key));
                    }
                }
                position = position << 1;
            }

            if ((hash & position) == 0) {
                PatriciaTrieSet<K> newLeft = left.add(hash, key);
                return new Node<K>(bits, depth, newLeft, right);
            } else {
                PatriciaTrieSet<K> newRight = right.add(hash, key);
                return new Node<K>(bits, depth, left, newRight);
            }
        }

        public boolean contains(int hash, K key) {
            int position = 1;
            for (int i = 0; i < depth; i++) {
                int hp = hash & position;
                int bp = bits & position;
                if (hp != bp) {
                    return false;
                }
                position = position << 1;
            }

            if ((hash & position) == 0) {
                return left.contains(hash, key);
            } else {
                return right.contains(hash, key);
            }
        }

        public PatriciaTrieSet<K> remove(K key) {
            int hash = key.hashCode();

            int position = 1;
            for (int i = 0; i < depth; i++) {
                int hp = hash & position;
                int bp = bits & position;
                if (hp != bp) {
                    return this;
                }
                position = position << 1;
            }

            if ((hash & position) == 0) {
                PatriciaTrieSet<K> newLeft = left.remove(key);
                if (newLeft == emptyInstance) {
                    return right;
                } else {
                    return new Node<K>(bits, depth, newLeft, right);
                }
            } else {
                PatriciaTrieSet<K> newRight = right.remove(key);
                if (newRight == emptyInstance) {
                    return left;
                } else {
                    return new Node<K>(bits, depth, left, newRight);
                }
            }
        }

        @Override
        protected LazyList<K> getEntrySeq() {
            return left.getEntrySeq().append(new Memoize<LazyList<K>>() {
                @Override
                protected LazyList<K> call() {
                    return right.getEntrySeq();
                }
            });
        }

    }

    static class Bucket<K> extends PatriciaTrieSet<K> {
        private final int bits;

        private final Object[] keys;

        public Bucket(int hash, K key) {
            bits = hash;
            keys = new Object[1];
            keys[0] = key;
        }

        public Bucket(int bits, Object[] keys) {
            this.bits = bits;
            this.keys = keys;
        }

        public PatriciaTrieSet<K> add(int hash, K key) {
            if (bits == hash) {
                return insertIntoThis(key);
            } else {
                return insertIntoNewNode(hash, key);
            }
        }

        private PatriciaTrieSet<K> insertIntoNewNode(int hash, K key) {
            int position = 1;
            for (int i = 0; i < 32; i++) {
                int hp = hash & position;
                int bp = bits & position;
                if (hp != bp) {
                    if (hp == 0) {
                        return new Node<K>(bits, i, new Bucket<K>(hash, key), this);
                    } else {
                        return new Node<K>(bits, i, this, new Bucket<K>(hash, key));
                    }
                }
                position = position << 1;
            }
            throw new IllegalStateException();
        }

        private PatriciaTrieSet<K> insertIntoThis(K key) {
            final int index = getIndex(key);
            if (index < keys.length) {
                return this;
            }

            final int newLength = keys.length + 1;
            final Object[] newKeys = Arrays.copyOf(keys, newLength);

            newKeys[newLength - 1] = key;

            return new Bucket<K>(bits, newKeys);
        }

        private int getIndex(K key) {
            for (int i = 0; i < keys.length; i++) {
                if (key.equals(keys[i])) {
                    return i;
                }
            }
            return keys.length;
        }

        public boolean contains(int hash, K key) {
            int index = getIndex(key);
            if (index < keys.length) {
                return true;
            }
            return false;
        }

        @Override
        public PatriciaTrieSet<K> remove(K key) {
            int index = getIndex(key);
            if (index >= keys.length) {
                return this;
            }
            if (keys.length == 1) {
                return emptyInstance;
            }
            final Object[] newKeys = removeElement(keys, index);
            return new Bucket<K>(bits, newKeys);
        }

        static Object[] removeElement(Object[] original, int element) {
            Object[] newArray = new Object[original.length - 1];
            System.arraycopy(original, 0, newArray, 0, element);
            System.arraycopy(original, element + 1, newArray, element, newArray.length - element);
            return newArray;
        }

        protected LazyList<K> getEntrySeq() {
            return getSeqFrom(0);
        }

        private LazyList<K> getSeqFrom(final int i) {
            if (i == keys.length) {
                return LazyList.<K>emptyStream();
            } else {

                return LazyList.cons((K) keys[i],

                    new Memoize<LazyList<K>>() {
                        @Override
                        protected LazyList<K> call() {
                            return getSeqFrom(i + 1);
                        }
                    });
            }
        }

    }

}
