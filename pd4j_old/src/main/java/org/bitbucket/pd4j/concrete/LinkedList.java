package org.bitbucket.pd4j.concrete;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.NoSuchElementException;
import org.bitbucket.pd4j.concept.Seq;
import org.bitbucket.pd4j.util.F1;
import org.bitbucket.pd4j.util.F2;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author pataky
 */
abstract class LinkedList<T> implements Seq<T, LinkedList<T>>, Iterable<T> {

    @Override
    public LinkedList<T> empty() {
        return emptyList();
    }

    @Override
    public LinkedList<T> cons(T t) {
        return new Cell<T>(t, this);
    }

    public static <T> LinkedList<T> emptyList() {
        return Empty.instance;
    }

    public static <T> LinkedList<T> create(T... values) {
        LinkedList<T> tail = emptyList();
        for (int i = values.length - 1; i >= 0; i--) {
            T t = values[i];
            tail = new Cell(t, tail);
        }
        return tail;
    }

    public LinkedList<T> revapp(LinkedList<T> other) {
        LinkedList<T> toRevApp = other;
        LinkedList<T> toReturn = this;
        while (!toRevApp.isEmpty()) {
            toReturn = toReturn.cons(toRevApp.head());
            toRevApp = toRevApp.tail();
        }
        return toReturn;
    }

    public <U> U fold(F2<U, U, T> fn, U u) {
        return SeqFn.foldL(this, u, fn);
    };

    public abstract <U> LinkedList<U> transform(F1<U, T> fn);

    static class Empty<T> extends LinkedList<T> {
        private static final Empty instance = new Empty();

        public LinkedList<T> tail() {
            throw new NoSuchElementException();
        }

        public T head() {
            throw new NoSuchElementException();
        }

        public boolean isEmpty() {
            return true;
        }

        public <U> LinkedList<U> transform(F1<U, T> fn) {
            return (LinkedList<U>) this;
        }

        @Override
        public String toString() {
            return "[]";
        }
    }

    static class Cell<T> extends LinkedList<T> {

        private final T value;
        private final LinkedList<T> tail;

        private Cell(T value, LinkedList<T> tail) {
            this.value = value;
            this.tail = tail;
        }

        public LinkedList<T> tail() {
            return tail;
        }

        public T head() {
            return value;
        }

        public boolean isEmpty() {
            return false;
        }

        public <U> LinkedList<U> transform(F1<U, T> fn) {
            ArrayList<U> revTrans = new ArrayList<U>();

            LinkedList<T> toTransform = this;
            while (!toTransform.isEmpty()) {
                revTrans.add(fn.apply(toTransform.head()));
                toTransform = toTransform.tail();
            }

            LinkedList<U> transformed = emptyList();
            for (int i = revTrans.size() - 1; i >= 0; i--) {
                transformed = transformed.cons(revTrans.get(i));
            }
            return transformed;
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder("[");
            sb.append(value);
            LinkedList<T> toPrint = tail();
            while (true) {
                if (toPrint.isEmpty()) {
                    sb.append("]");
                    break;
                } else {
                    sb.append(", ");
                    sb.append(toPrint.head());
                }
                toPrint = toPrint.tail();
            }
            return sb.toString();
        }
    }

    public Iterator<T> iterator() {
        return new SeqIterator<T>(this);
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (!(other instanceof LinkedList)) {
            return false;
        }
        return SeqFn.eq(this, (LinkedList) other);
    }

}
