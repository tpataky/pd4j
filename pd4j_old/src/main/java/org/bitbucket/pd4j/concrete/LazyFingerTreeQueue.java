/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.pd4j.concrete;

import java.util.Iterator;
import org.bitbucket.pd4j.concept.Deque;
import java.util.NoSuchElementException;
import java.util.function.Supplier;
import static org.bitbucket.pd4j.concrete.FingerTreeUtils.*;
import static org.bitbucket.pd4j.concrete.ArrayUtils.*;

/**
 *
 * @author pataky
 */
public abstract class LazyFingerTreeQueue<T> implements Deque<T, LazyFingerTreeQueue<T>>, Iterable<T> {

    public static <T> LazyFingerTreeQueue<T> emptyQueue() {
        return empty;
    }

    @Override
    public LazyFingerTreeQueue<T> empty() {
        return empty;
    }

    public abstract LazyFingerTreeQueue<T> append(LazyFingerTreeQueue<T> other);

    private final static Empty empty = new Empty();

    private final static Supplier emptySupplier = () -> empty;

    public Iterator<T> iterator() {
        return new SeqIterator<T>(this);
    }

    static class Empty<T> extends LazyFingerTreeQueue<T> {
        @Override
        public LazyFingerTreeQueue<T> cons(T t) {
            return new Single<T>(t);
        }

        @Override
        public T head() {
            throw new NoSuchElementException();
        }

        @Override
        public LazyFingerTreeQueue<T> tail() {
            throw new NoSuchElementException();
        }

        @Override
        public LazyFingerTreeQueue<T> snoc(T t) {
            return new Single<T>(t);
        }

        @Override
        public LazyFingerTreeQueue<T> init() {
            throw new NoSuchElementException();
        }

        @Override
        public T last() {
            throw new NoSuchElementException();
        }

        @Override
        public boolean isEmpty() {
            return true;
        }

        @Override
        public LazyFingerTreeQueue<T> append(LazyFingerTreeQueue<T> other) {
            return other;
        }

    }

    static class Single<T> extends LazyFingerTreeQueue<T> {
        final T elem;

        public Single(T elem) {
            this.elem = elem;
        }

        @Override
        public LazyFingerTreeQueue<T> cons(T t) {
            return new Deep<T>(array(t), emptySupplier, empty, array(this.elem));
        }

        @Override
        public T head() {
            return elem;
        }

        @Override
        public LazyFingerTreeQueue<T> tail() {
            return empty;
        }

        @Override
        public LazyFingerTreeQueue<T> snoc(T t) {
            return new Deep<T>(array(this.elem), emptySupplier, empty, array(t));
        }

        @Override
        public LazyFingerTreeQueue<T> init() {
            return empty;
        }

        @Override
        public T last() {
            return elem;
        }

        @Override
        public boolean isEmpty() {
            return false;
        }

        @Override
        public LazyFingerTreeQueue<T> append(LazyFingerTreeQueue<T> other) {
            return other.cons(elem);
        }

    }

    static class Deep<T> extends LazyFingerTreeQueue<T> {
        final Object[] left;
        final Supplier<LazyFingerTreeQueue<Node<T>>> supplier;
        LazyFingerTreeQueue<Node<T>> value;
        final Object[] right;

        public Deep(Object[] left, Supplier<LazyFingerTreeQueue<Node<T>>> treeSupplier, LazyFingerTreeQueue<Node<T>> treeValue, Object[] right) {
            this.left = left;
            this.supplier = treeSupplier;
            this.value = treeValue;
            this.right = right;
        }

        @Override
        public LazyFingerTreeQueue<T> cons(T t) {
            if (left.length == 4) {
                return new Deep<>(array(t, left[0]),
                        () -> tree().cons(new Node3<>((T) left[1], (T) left[2], (T) left[3])),
                        null,
                        right);
            } else {
                return new Deep<>(appL(t, left), supplier, value, right);
            }
        }

        @Override
        public T head() {
            return (T) left[0];
        }

        @Override
        public LazyFingerTreeQueue<T> tail() {
            if (left.length > 1) {
                return new Deep<>(popL(left), supplier, value, right);
            }

            if (tree() == empty) {
                if (right.length == 1) {
                    return new Single<>((T) right[0]);
                } else {
                    return new Deep<>(array(right[0]), supplier, value, popL(right));
                }
            } else {
                Node<T> n = tree().head();
                if (n instanceof Node2) {
                    Node2 n2 = (Node2) n;
                    return new Deep<>(array(n2.left, n2.right), () -> tree().tail(), null, right);
                } else {
                    Node3 n3 = (Node3) n;
                    return new Deep<>(array(n3.left, n3.mid, n3.right), () -> tree().tail(), null, right);
                }
            }
        }

        @Override
        public LazyFingerTreeQueue<T> snoc(T t) {
            if (right.length == 4) {
                return new Deep<>(left, () -> tree().snoc(new Node3<>((T) right[0], (T) right[1], (T) right[2])), null, array(right[3], t));
            } else {
                return new Deep<>(left, supplier, value, appR(right, t));
            }
        }

        @Override
        public LazyFingerTreeQueue<T> init() {
            if (right.length > 1) {
                return new Deep<T>(left, supplier, value, popR(right));
            }

            if (tree() == empty) {
                if (left.length == 1) {
                    return new Single<T>((T) left[0]);
                } else {
                    return new Deep<T>(popR(left), supplier, value, array(left[left.length - 1]));
                }
            } else {
                Node<T> n = tree().last();
                if (n instanceof Node2) {
                    Node2 n2 = (Node2) n;
                    return new Deep<T>(left, () -> tree().init(), null, array(n2.left, n2.right));
                } else {
                    Node3 n3 = (Node3) n;
                    return new Deep<T>(left, () -> tree().init(), null, array(n3.left, n3.mid, n3.right));
                }
            }
        }

        @Override
        public T last() {
            return (T) right[right.length - 1];
        }

        @Override
        public boolean isEmpty() {
            return false;
        }

        @Override
        public LazyFingerTreeQueue<T> append(LazyFingerTreeQueue<T> other) {
            if (other instanceof Empty) {
                return this;
            }
            if (other instanceof Single) {
                Single<T> s = (Single<T>) other;
                return snoc(s.elem);
            }
            Deep<T> d = (Deep<T>) other;
            Object[] app = app(right, d.left);
            switch (app.length) {
                case 2:
                    return new Deep<T>(left, () -> tree().snoc(new Node2<T>((T) app[0], (T) app[1])).append(d.tree()), null, right);
                case 3:
                    return new Deep<T>(left, () -> tree().snoc(new Node3<T>((T) app[0], (T) app[1], (T) app[2])).append(d.tree()), null, right);
                case 4:
                    return new Deep<T>(left, () -> tree()
                            .snoc(new Node2<T>((T) app[0], (T) app[1]))
                            .snoc(new Node2<T>((T) app[2], (T) app[3]))
                            .append(d.tree()),
                            null,
                            right);
                case 5:
                    return new Deep<T>(left, () -> tree()
                            .snoc(new Node3<T>((T) app[0], (T) app[1], (T) app[2]))
                            .snoc(new Node2<T>((T) app[3], (T) app[4]))
                            .append(d.tree()),
                            null,
                            right);
                case 6:
                    return new Deep<T>(left, () -> tree()
                            .snoc(new Node3<T>((T) app[0], (T) app[1], (T) app[2]))
                            .snoc(new Node3<T>((T) app[3], (T) app[4], (T) app[5]))
                            .append(d.tree()),
                            null,
                            right);
                case 7:
                    return new Deep<T>(left, () -> tree()
                            .snoc(new Node3<T>((T) app[0], (T) app[1], (T) app[2]))
                            .snoc(new Node2<T>((T) app[3], (T) app[4]))
                            .snoc(new Node2<T>((T) app[5], (T) app[6]))
                            .append(d.tree()),
                            null,
                            right);
                case 8:
                    return new Deep<T>(left, () -> tree()
                            .snoc(new Node3<T>((T) app[0], (T) app[1], (T) app[2]))
                            .snoc(new Node3<T>((T) app[3], (T) app[4], (T) app[5]))
                            .snoc(new Node2<T>((T) app[6], (T) app[7]))
                            .append(d.tree()),
                            null,
                            right);
            }
            throw new IllegalStateException();
        }

        private LazyFingerTreeQueue<Node<T>> tree() {
            if (value == null) {
                value = supplier.get();
            }
            return value;
        }
    }

}
