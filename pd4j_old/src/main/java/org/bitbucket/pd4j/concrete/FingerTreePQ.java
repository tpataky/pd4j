/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.pd4j.concrete;

import java.util.Iterator;
import org.bitbucket.pd4j.concept.Deque;
import org.bitbucket.pd4j.concept.PriorityQueue;
import static org.bitbucket.pd4j.concrete.FingerTreeUtils.*;
import static org.bitbucket.pd4j.concrete.ArrayUtils.*;
import java.util.Comparator;
import java.util.NoSuchElementException;

/**
 *
 * @author pataky
 */
public class FingerTreePQ<T, Ord extends Comparator<T>> implements
        PriorityQueue<T, Ord, FingerTreePQ<T, Ord>>,
        Deque<T, FingerTreePQ<T, Ord>>,
        Iterable<T>
{

    private final Ord ord;
    private final MeasuredFT impl;

    public static <T, Ord extends Comparator<T>> FingerTreePQ<T, Ord> emptyQueue(Ord ord) {
        return new FingerTreePQ<T, Ord>(ord);
    }

    public static <T extends Comparable<T>> FingerTreePQ<T, NatOrd<T>> emptyQueue() {
        return new FingerTreePQ<T, NatOrd<T>>(NatOrd.<T>getInstance());
    }

    private FingerTreePQ(Ord ord) {
        this.ord = ord;
        this.impl = new Empty();
    }

    private FingerTreePQ(Ord ord, MeasuredFT impl) {
        this.ord = ord;
        this.impl = impl;
    }

    public FingerTreePQ<T, Ord> empty() {
        return new FingerTreePQ<T, Ord>(ord, new Empty());
    }

    public T peek() {
        return (T) impl.peek().value();
    }

    public FingerTreePQ<T, Ord> pop() {
        return new FingerTreePQ<T, Ord>(ord, impl.pop());
    }

    public FingerTreePQ<T, Ord> push(T t) {
        return new FingerTreePQ<T, Ord>(ord, impl.push(new Measured(t)));
    }

    public boolean isEmpty() {
        return impl.isEmpty();
    }

    public T last() {
        return (T) impl.last().value();
    }

    public FingerTreePQ<T, Ord> init() {
        return new FingerTreePQ<T, Ord>(ord, impl.init());
    }

    public FingerTreePQ<T, Ord> snoc(T t) {
        return new FingerTreePQ<T, Ord>(ord, impl.snoc(new Measured(t)));
    }

    public T head() {
        return (T) impl.head().value();
    }

    public FingerTreePQ<T, Ord> tail() {
        return new FingerTreePQ<T, Ord>(ord, impl.tail());
    }

    public FingerTreePQ<T, Ord> cons(T t) {
        return new FingerTreePQ<T, Ord>(ord, impl.cons(new Measured(t)));
    }

    public Iterator<T> iterator() {
        return new PriorityQueueIterator<T>(this);
    }

    @Override
    public String toString() {
        return impl.toString();
    }

    class Split<T, F> {
        public final F left;
        public final T elem;
        public final F right;

        public Split(F left, T elem, F right) {
            this.left = left;
            this.elem = elem;
            this.right = right;
        }
    }

    abstract class MeasuredFT<V, T extends Measurable<V, T>, Ord extends Comparator<V>> implements Measurable<V, MeasuredFT<V, T, Ord>> {

        public abstract T peek();

        public abstract MeasuredFT<V, T, Ord> pop();

        public MeasuredFT<V, T, Ord> push(T t) {
            return snoc(t);
        }

        public abstract boolean isEmpty();

        public abstract T last();

        public abstract MeasuredFT<V, T, Ord> init();

        public abstract MeasuredFT<V, T, Ord> snoc(T t);

        public abstract T head();

        public abstract MeasuredFT<V, T, Ord> tail();

        public abstract MeasuredFT<V, T, Ord> cons(T t);

        public abstract Split<T, MeasuredFT<V, T, Ord>> split();

        public MeasuredFT<V, T, Ord> fromList(Object[] list) {
            MeasuredFT<V, T, Ord> tree = new Empty<V, T, Ord>();
            for (Object o : list) {
                T t = (T) o;
                tree = tree.snoc(t);
            }
            return tree;
        }

        public abstract MeasuredFT<V, T, Ord> append(MeasuredFT<V, T, Ord> other);
    }

    class Empty<V, T extends Measurable<V, T>, Ord extends Comparator<V>> extends MeasuredFT<V, T, Ord> {

        @Override
        public T peek() {
            throw new NoSuchElementException();
        }

        @Override
        public MeasuredFT<V, T, Ord> pop() {
            throw new NoSuchElementException();
        }

        @Override
        public boolean isEmpty() {
            return true;
        }

        @Override
        public T last() {
            throw new NoSuchElementException();
        }

        @Override
        public MeasuredFT<V, T, Ord> init() {
            throw new NoSuchElementException();
        }

        @Override
        public MeasuredFT<V, T, Ord> snoc(T t) {
            return new Single<V, T, Ord>(t);
        }

        @Override
        public T head() {
            throw new NoSuchElementException();
        }

        @Override
        public MeasuredFT<V, T, Ord> tail() {
            throw new NoSuchElementException();
        }

        @Override
        public MeasuredFT<V, T, Ord> cons(T t) {
            return new Single<V, T, Ord>(t);
        }

        @Override
        public Split<T, MeasuredFT<V, T, Ord>> split() {
            throw new NoSuchElementException();
        }

        @Override
        public MeasuredFT<V, T, Ord> append(MeasuredFT<V, T, Ord> other) {
            return other;
        }

        public V measure() {
            throw new NoSuchElementException();
        }

        public MeasuredFT<V, T, Ord> value() {
            throw new NoSuchElementException();
        }

        @Override
        public String toString() {
            return "{}";
        }

    }

    class Single<V, T extends Measurable<V, T>, Ord extends Comparator<V>> extends MeasuredFT<V, T, Ord> {
        final V max;
        final T elem;

        public Single(T elem) {
            this.elem = elem;
            this.max = elem.measure();
        }

        @Override
        public MeasuredFT<V, T, Ord> cons(T t) {
            return new Deep<V, T, Ord>(t, elem);
        }

        @Override
        public T head() {
            return elem;
        }

        @Override
        public MeasuredFT<V, T, Ord> init() {
            return new Empty<V, T, Ord>();
        }

        @Override
        public boolean isEmpty() {
            return false;
        }

        @Override
        public T last() {
            return elem;
        }

        @Override
        public T peek() {
            return elem;
        }

        @Override
        public MeasuredFT<V, T, Ord> pop() {
            return new Empty<V, T, Ord>();
        }

        @Override
        public MeasuredFT<V, T, Ord> snoc(T t) {
            return new Deep<V, T, Ord>(elem, t);
        }

        @Override
        public MeasuredFT<V, T, Ord> tail() {
            return new Empty<V, T, Ord>();
        }

        @Override
        public Split<T, MeasuredFT<V, T, Ord>> split() {
            final Empty<V, T, Ord> empty = new Empty<V, T, Ord>();
            return new Split<T, MeasuredFT<V, T, Ord>>(empty, elem, empty);
        }

        @Override
        public MeasuredFT<V, T, Ord> append(MeasuredFT<V, T, Ord> other) {
            return other.cons(elem);
        }

        public V measure() {
            return max;
        }

        public MeasuredFT<V, T, Ord> value() {
            return this;
        }

        @Override
        public String toString() {
            return "{" + elem + '}';
        }

    }

    class Deep<V, T extends Measurable<V, T>, Ord extends Comparator<V>> extends MeasuredFT<V, T, Ord> {
        final Object[] left;
        final MeasuredFT<V, MeasuredNode<V, T>, Ord> tree;
        final Object[] right;
        final V min;

        public Deep(T left, T right) {
            this(array(left), new Empty(), array(right));
        }

        public Deep(Object[] left, MeasuredFT<V, MeasuredNode<V, T>, Ord> tree, Object[] right) {
            this.left = left;
            this.tree = tree;
            this.right = right;
            this.min = minOf(left, tree, right);
        }

        private V minOf(Object[] left, MeasuredFT<V, MeasuredNode<V, T>, Ord> tree, Object[] right) {
            V minOfDigits = minOf(minOf(left), minOf(right));
            if (tree instanceof Empty) {
                return minOfDigits;
            } else {
                return minOf(minOfDigits, tree.measure());
            }
        }

        private V minOf(Object[] list) {
            V min = null;
            for (Object o : list) {
                T t = (T) o;
                min = minOf(min, t.measure());
            }
            return min;
        }

        private V minOf(V a, V b) {
            if (a == null) {
                return b;
            }
            int cmp = ((Comparator<V>) ord).compare(a, b);
            return (cmp <= 0) ? a : b;
        }

        @Override
        public T peek() {
            return split().elem;
        }

        @Override
        public MeasuredFT<V, T, Ord> pop() {
            Split<T, MeasuredFT<V, T, Ord>> split = split();
            return split.left.append(split.right);
        }

        @Override
        public Split<T, MeasuredFT<V, T, Ord>> split() {
            if (tree instanceof Empty) {
                Split<T, Object[]> splitDigit = splitDigit(app(left, right));
                return new Split<T, MeasuredFT<V, T, Ord>>(fromList(splitDigit.left), splitDigit.elem, fromList(splitDigit.right));
            }

            Split<T, Object[]> splitLeft = splitDigit(left);
            if (splitLeft != null) {
                MeasuredFT<V, T, Ord> l = fromList(splitLeft.left);
                T x = splitLeft.elem;
                MeasuredFT<V, T, Ord> r;
                if (splitLeft.right.length == 0) {
                    r = new Deep<V, T, Ord>(tree.head().toList(), tree.tail(), right);
                } else {
                    r = new Deep<V, T, Ord>(splitLeft.right, tree, right);
                }
                return new Split<T, MeasuredFT<V, T, Ord>>(l, x, r);
            }

            Split<T, Object[]> splitRight = splitDigit(right);
            if (splitRight != null) {
                MeasuredFT<V, T, Ord> r = fromList(splitRight.right);
                T x = splitRight.elem;
                MeasuredFT<V, T, Ord> l;
                if (splitRight.left.length == 0) {
                    l = new Deep<V, T, Ord>(left, tree.init(), tree.last().toList());
                } else {
                    l = new Deep<V, T, Ord>(left, tree, splitRight.left);
                }
                return new Split<T, MeasuredFT<V, T, Ord>>(l, x, r);
            }

            Split<MeasuredNode<V, T>, MeasuredFT<V, MeasuredNode<V, T>, Ord>> split = tree.split();
            Split<T, Object[]> splitMid = splitDigit(split.elem.toList());
            T x = splitMid.elem;

            MeasuredFT<V, T, Ord> l;
            if (split.left instanceof Empty) {
                l = fromList(app(left, splitMid.left));
            } else if (splitMid.left.length == 0) {
                l = new Deep<V, T, Ord>(left, split.left.init(), split.left.last().toList());
            } else {
                l = new Deep<V, T, Ord>(left, split.left, splitMid.left);
            }

            MeasuredFT<V, T, Ord> r;
            if (split.right instanceof Empty) {
                r = fromList(app(splitMid.right, right));
            } else if (splitMid.right.length == 0) {
                r = new Deep<V, T, Ord>(split.right.head().toList(), split.right.tail(), right);
            } else {
                r = new Deep<V, T, Ord>(splitMid.right, split.right, right);
            }

            return new Split<T, MeasuredFT<V, T, Ord>>(l, x, r);
        }

        private Split<T, Object[]> splitDigit(Object[] digits) {
            Object[] left = new Object[0];
            T x = null;
            Object[] right = left;

            boolean toLeft = true;
            for (Object o : digits) {
                T t = (T) o;
                int cmp = ((Comparator<V>) ord).compare(min, t.measure());
                if (cmp == 0) {
                    x = t;
                    toLeft = false;
                } else {
                    if (toLeft) {
                        left = appR(left, t);
                    } else {
                        right = appR(right, t);
                    }
                }
            }
            if (toLeft) {
                return null;
            } else {
                return new Split<T, Object[]>(left, x, right);
            }
        }

        @Override
        public boolean isEmpty() {
            return false;
        }

        @Override
        public T last() {
            return (T) right[right.length - 1];
        }

        @Override
        public MeasuredFT<V, T, Ord> init() {
            if (right.length > 1) {
                return new Deep<V, T, Ord>(left, tree, popR(right));
            }

            if (tree instanceof Empty) {
                if (left.length == 1) {
                    return new Single<V, T, Ord>((T) left[0]);
                } else {
                    return new Deep<V, T, Ord>(popR(left), tree, array(left[left.length - 1]));
                }
            } else {
                MeasuredNode<V, T> n = tree.last();
                if (n instanceof MeasuredNode2) {
                    MeasuredNode2<V, T> n2 = (MeasuredNode2) n;
                    return new Deep<V, T, Ord>(left, tree.init(), array(n2.left, n2.right));
                } else {
                    MeasuredNode3<V, T> n3 = (MeasuredNode3) n;
                    return new Deep<V, T, Ord>(left, tree.init(), array(n3.left, n3.mid, n3.right));
                }
            }
        }

        @Override
        public MeasuredFT<V, T, Ord> snoc(T t) {
            if (right.length == 4) {
                final V minOf = minOf(
                                    minOf(
                                        ((T) right[0]).measure(),
                                        ((T) right[1]).measure()),
                                    ((T) right[2]).measure());

                return new Deep<V, T, Ord>(left, tree.snoc(new MeasuredNode3<V, T>(minOf, (T) right[0], (T) right[1], (T) right[2])), array(right[3], t));
            } else {
                return new Deep<V, T, Ord>(left, tree, appR(right, t));
            }
        }

        @Override
        public T head() {
            return (T) left[0];
        }

        @Override
        public MeasuredFT<V, T, Ord> tail() {
            if (left.length > 1) {
                return new Deep<V, T, Ord>(popL(left), tree, right);
            }

            if (tree instanceof Empty) {
                if (right.length == 1) {
                    return new Single<V, T, Ord>((T) right[0]);
                } else {
                    return new Deep<V, T, Ord>(array(right[0]), tree, popL(right));
                }
            } else {
                MeasuredNode<V, T> n = tree.head();
                if (n instanceof MeasuredNode2) {
                    MeasuredNode2<V, T> n2 = (MeasuredNode2) n;
                    return new Deep<V, T, Ord>(array(n2.left, n2.right), tree.tail(), right);
                } else {
                    MeasuredNode3<V, T> n3 = (MeasuredNode3) n;
                    return new Deep<V, T, Ord>(array(n3.left, n3.mid, n3.right), tree.tail(), right);
                }
            }
        }

        @Override
        public MeasuredFT<V, T, Ord> cons(T t) {
            if (left.length == 4) {
                final V maxOf = minOf(
                                    minOf(
                                        ((T) left[1]).measure(),
                                        ((T) left[2]).measure()),
                                    ((T) left[3]).measure());

                return new Deep<V, T, Ord>(array(t, left[0]), tree.cons(new MeasuredNode3<V, T>(maxOf, (T) left[1], (T) left[2], (T) left[3])), right);
            } else {
                return new Deep<V, T, Ord>(appL(t, left), tree, right);
            }
        }

        @Override
        public MeasuredFT<V, T, Ord> append(MeasuredFT<V, T, Ord> other) {
            if (other instanceof Empty) {
                return this;
            }
            if (other instanceof Single) {
                Single<V, T, Ord> s = (Single<V, T, Ord>) other;
                return snoc(s.elem);
            }
            Deep<V, T, Ord> d = (Deep<V, T, Ord>) other;
            Object[] app = app(right, d.left);
            switch (app.length) {
                case 2:
                    return new Deep<V, T, Ord>(left, tree
                            .snoc(new MeasuredNode2<V, T>(minOf(((T) app[0]).measure(), ((T) app[1]).measure()), (T) app[0], (T) app[1]))
                            .append(d.tree), d.right);
                case 3:
                    return new Deep<V, T, Ord>(left, tree
                            .snoc(new MeasuredNode3<V, T>(minOf(minOf(((T) app[0]).measure(), ((T) app[1]).measure()), ((T) app[2]).measure()), (T) app[0], (T) app[1], (T) app[2]))
                            .append(d.tree), d.right);
                case 4:
                    return new Deep<V, T, Ord>(left, tree
                            .snoc(new MeasuredNode2<V, T>(minOf(((T) app[0]).measure(), ((T) app[1]).measure()), (T) app[0], (T) app[1]))
                            .snoc(new MeasuredNode2<V, T>(minOf(((T) app[2]).measure(), ((T) app[3]).measure()), (T) app[2], (T) app[3]))
                            .append(d.tree), d.right);
                case 5:
                    return new Deep<V, T, Ord>(left, tree
                            .snoc(new MeasuredNode3<V, T>(minOf(minOf(((T) app[0]).measure(), ((T) app[1]).measure()), ((T) app[2]).measure()), (T) app[0], (T) app[1], (T) app[2]))
                            .snoc(new MeasuredNode2<V, T>(minOf(((T) app[3]).measure(), ((T) app[4]).measure()), (T) app[3], (T) app[4]))
                            .append(d.tree), d.right);
                case 6:
                    return new Deep<V, T, Ord>(left, tree
                            .snoc(new MeasuredNode3<V, T>(minOf(minOf(((T) app[0]).measure(), ((T) app[1]).measure()), ((T) app[2]).measure()), (T) app[0], (T) app[1], (T) app[2]))
                            .snoc(new MeasuredNode3<V, T>(minOf(minOf(((T) app[3]).measure(), ((T) app[4]).measure()), ((T) app[5]).measure()), (T) app[3], (T) app[4], (T) app[5]))
                            .append(d.tree), d.right);
                case 7:
                    return new Deep<V, T, Ord>(left, tree
                            .snoc(new MeasuredNode3<V, T>(minOf(minOf(((T) app[0]).measure(), ((T) app[1]).measure()), ((T) app[2]).measure()), (T) app[0], (T) app[1], (T) app[2]))
                            .snoc(new MeasuredNode2<V, T>(minOf(((T) app[3]).measure(), ((T) app[4]).measure()), (T) app[3], (T) app[4]))
                            .snoc(new MeasuredNode2<V, T>(minOf(((T) app[5]).measure(), ((T) app[6]).measure()), (T) app[5], (T) app[6]))
                            .append(d.tree), d.right);
                case 8:
                    return new Deep<V, T, Ord>(left, tree
                            .snoc(new MeasuredNode3<V, T>(minOf(minOf(((T) app[0]).measure(), ((T) app[1]).measure()), ((T) app[2]).measure()), (T) app[0], (T) app[1], (T) app[2]))
                            .snoc(new MeasuredNode3<V, T>(minOf(minOf(((T) app[3]).measure(), ((T) app[4]).measure()), ((T) app[5]).measure()), (T) app[3], (T) app[4], (T) app[5]))
                            .snoc(new MeasuredNode2<V, T>(minOf(((T) app[6]).measure(), ((T) app[7]).measure()), (T) app[6], (T) app[7]))
                            .append(d.tree), d.right);
            }
            throw new IllegalStateException();
        }

        public V measure() {
            return min;
        }

        public MeasuredFT<V, T, Ord> value() {
            return this;
        }

        @Override
        public String toString() {
            StringBuilder builder = new StringBuilder();
            builder.append("{[");
            for (Object o : left) {
                builder.append(o.toString());
                builder.append(", ");
            }
            builder.delete(builder.length() - 2, builder.length());
            builder.append("] ");
            builder.append(tree.toString());
            builder.append(" [");
            for (Object o : right) {
                builder.append(o.toString());
                builder.append(", ");
            }
            builder.delete(builder.length() - 2, builder.length());
            builder.append("]}");
            return builder.toString();
        }

    }
}
