package org.bitbucket.pd4j.concrete;

import java.util.Arrays;
import java.util.NoSuchElementException;
import org.bitbucket.pd4j.concept.Seq;
import org.bitbucket.pd4j.util.Option;
import org.bitbucket.pd4j.util.Pair;
import org.bitbucket.pd4j.util.F2;
import org.bitbucket.pd4j.util.F1;
import org.bitbucket.pd4j.util.Memoize;

public abstract class LazyList<T> implements Seq<T, LazyList<T>> {

    private final static LazyList EMPTY_STREAM = new Empty();

    public static <T> LazyList<T> emptyStream() {
        return EMPTY_STREAM;
    }

    public static <T> LazyList<T> fromVarArgs(T... values) {
        return fromArray(0, values);
    }

    public static <T> LazyList<T> fromArray(T[] values) {
        return fromArray(0, values);
    }

    private static <T> LazyList<T> fromArray(final int idx, final T[] values) {
        return new ArrayCell<T>(values, idx);
    }

    public static <State, T> LazyList<T> unfold(State state, final F1<? extends Option<Pair<T, State>>, State> fn) {
        Option<Pair<T, State>> o = fn.apply(state);
        if (o.isNothing()) {
            return emptyStream();
        }
        Option.Some<Pair<T, State>> some = (Option.Some<Pair<T, State>>) o;
        final State newState = some.value.second;
        final T t = some.value.first;


        return new Cell(t, new Memoize<LazyList<T>>() {
            protected LazyList<T> call() {
                return unfold(newState, fn);
            }
        });
    }

    public LazyList<T> empty() {
        return LazyList.emptyStream();
    }

    public static <T> LazyList<T> cons(T t, Memoize<LazyList<T>> tail) {
        return new Cell<T>(t, tail);
    }

    public LazyList<T> cons(T t) {
        return new Cell<T>(t, Memoize.val(this));
    }

    public <U> U fold(F2<U, U, T> fn, U u) {
        return SeqFn.foldL(this, u, fn);
    };

    public abstract LazyList<T> append(LazyList<T> other);
    public abstract LazyList<T> append(Memoize<LazyList<T>> other);
    public abstract LazyList<T> reverse();
    public abstract LazyList<T> take(int n);

    public abstract <U> LazyList<U> transform(F1<U, T> fn);

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (!(other instanceof LazyList)) {
            return false;
        }
        return SeqFn.eq(this, (LazyList) other);
    }

    static class Empty<T> extends LazyList<T> {

        @Override
        public T head() {
            throw new UnsupportedOperationException();
        }

        @Override
        public LazyList<T> tail() {
            throw new UnsupportedOperationException();
        }

        @Override
        public LazyList<T> append(LazyList<T> other) {
            return other;
        }

        @Override
        public LazyList<T> append(Memoize<LazyList<T>> other) {
            return other.get();
        }

        @Override
        public LazyList<T> reverse() {
            return this;
        }

        @Override
        public LazyList<T> take(int n) {
            if (n > 0) {
                throw new NoSuchElementException();
            }
            return this;
        }

        public boolean isEmpty() {
            return true;
        }

        @Override
        public <U> LazyList<U> transform(F1<U, T> fn) {
            return (LazyList<U>) this;
        }

    }

    static class Cell<T> extends LazyList<T> {

        private final T value;
        private final Memoize<LazyList<T>> tail;

        Cell(T value, Memoize<LazyList<T>> tail) {
            this.value = value;
            this.tail = tail;
        }

        @Override
        public T head() {
            return value;
        }

        @Override
        public LazyList<T> tail() {
            return tail.get();
        }

        @Override
        public LazyList<T> append(final LazyList<T> other) {
            return new Cell(value, new Memoize<LazyList<T>>() {
                protected LazyList<T> call() {
                    return tail().append(other);
                }
            });
        }

        @Override
        public LazyList<T> append(final Memoize<LazyList<T>> other) {
            return new Cell(value, new Memoize<LazyList<T>>() {
                protected LazyList<T> call() {
                    return tail().append(other);
                }
            });
        }

        @Override
        public LazyList<T> reverse() {
            LazyList<T> acc = emptyStream();
            LazyList<T> remaining = this;
            while (true) {
                acc = acc.cons(remaining.head());

                remaining = remaining.tail();
                if (remaining instanceof Empty) {
                    return acc;
                }
            }
        }

        @Override
        public LazyList<T> take(final int n) {
            if (n <= 0) {
                return emptyStream();
            }

            return new Cell(value, new Memoize<LazyList<T>>() {
                protected LazyList<T> call() {
                    return tail().take(n - 1);
                }
            });
        }

        public boolean isEmpty() {
            return false;
        }

        @Override
        public <U> LazyList<U> transform(final F1<U, T> fn) {
            return new Cell<U>(fn.apply(value), Memoize.fn(() -> tail().transform(fn)));
        }

    }

    private static class ArrayCell<T> extends LazyList<T> {
        private final Object[] array;
        private final int index;

        public ArrayCell(Object[] array, int index) {
            this.array = array;
            this.index = index;
        }

        @Override
        public LazyList<T> append(final LazyList<T> other) {
            return new Cell(array[index], new Memoize<LazyList<T>>() {
                protected LazyList<T> call() {
                    return tail().append(other);
                }
            });
        }

        @Override
        public LazyList<T> append(final Memoize<LazyList<T>> other) {
            return new Cell(array[index], new Memoize<LazyList<T>>() {
                protected LazyList<T> call() {
                    return tail().append(other);
                }
            });
        }

        @Override
        public LazyList<T> reverse() {
            Object[] reverted = new Object[array.length - index];
            for (int i = array.length - 1; i >= index; i--) {
                reverted[i] = array[array.length - 1 - index];
            }
            return new ArrayCell<T>(reverted, 0);
        }

        @Override
        public LazyList<T> take(int n) {
            if (index + n > array.length) {
                throw new NoSuchElementException();
            }
            if (index + n == array.length) {
                return this;
            }
            if (n == 0) {
                return EMPTY_STREAM;
            }

            Object[] taken = Arrays.copyOfRange(array, 0, n);
            return new ArrayCell<T>(taken, 0);
        }

        public T head() {
            return (T) array[index];
        }

        public LazyList<T> tail() {
            if (index + 1 == array.length) {
                return EMPTY_STREAM;
            }
            return new ArrayCell<T>(array, index + 1);
        }

        public boolean isEmpty() {
            return false;
        }

        @Override
        public <U> LazyList<U> transform(final F1<U, T> fn) {
            return new Cell(fn.apply(head()), Memoize.fn(() -> tail().transform(fn)));
        }
    }
}
