/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.pd4j.concrete;

import java.util.Iterator;
import org.bitbucket.pd4j.concept.Vector;
import java.util.Arrays;
import org.bitbucket.pd4j.concept.Seq;
import org.bitbucket.pd4j.util.Memoize;

/**
 *
 * @author pataky
 */
public abstract class TreeVector<T> implements Vector<T, TreeVector<T>>, Iterable<T> {
    private static final int BLOCK_SIZE = 32;
    private static final TreeVector EMPTY_INSTANCE = new InternalNode();

    public static <T> TreeVector<T> emptyVector() {
        return EMPTY_INSTANCE;
    }

    public static <T> TreeVector<T> create(int size) {
        return new InternalNode(size);
    }

    @Override
    public TreeVector<T> empty() {
        return emptyVector();
    }

    @Override
    public T get(int i) {
        checkBounds(i);
        return doGet(i);
    }

    @Override
    public TreeVector<T> set(int i, T t) {
        checkBounds(i);
        return doSet(i, t);
    }

    public Seq<T, ?> entrySeq() {
        return getEntrySeq().take(size());
    }

    protected abstract LazyList<T> getEntrySeq();

    public Iterator<T> iterator() {
        return new SeqIterator<T>(entrySeq());
    }

    private void checkBounds(int i) throws IndexOutOfBoundsException {
        if (i < 0 || i >= size()) {
            throw new IndexOutOfBoundsException();
        }
    }

    protected abstract T doGet(int i);
    protected abstract TreeVector<T> doSet(int i, T t);

    private static class InternalNode<T> extends TreeVector<T> {
        protected final int size;
        protected final int chunkSize;
        private final TreeVector<T>[] children;

        private InternalNode(int size, int chunkSize, TreeVector<T>[] children) {
            this.size = size;
            this.chunkSize = chunkSize;
            this.children = children;
        }

        private InternalNode() {
            this.size = 0;
            this.chunkSize = BLOCK_SIZE;
            this.children = new TreeVector[BLOCK_SIZE];
        }

        private InternalNode(int size) {
            this.size = size;
            this.chunkSize = getChunkSize(size);
            this.children = createChildren(size, chunkSize);
        }

        private TreeVector<T>[] createChildren(int size, int chunkSize) {
            TreeVector<T>[] newChildren = new TreeVector[BLOCK_SIZE];
            int childNum = getChildNum(size, chunkSize);
            for (int i = 0; i < childNum; i++) {
                newChildren[i] = createChild(chunkSize);
            }
            return newChildren;
        }

        private TreeVector<T> createChild(int chunkSize) {
            if (chunkSize > BLOCK_SIZE) {
                return new InternalNode<T>(chunkSize);
            } else {
                return new Leaf<T>();
            }
        }

        private int getChildNum(int size, int chunkSize) {
            int childNum = (size + chunkSize - 1) / chunkSize;
            return childNum;
        }

        private int getChunkSize(int size) {
            int calculatedChunkSize = 1;
            while (true) {
                calculatedChunkSize = calculatedChunkSize * BLOCK_SIZE;
                if (calculatedChunkSize * BLOCK_SIZE >= size) {
                    return calculatedChunkSize;
                }
            }
        }

        @Override
        public TreeVector<T> resize(int newSize) {
            if (newSize > size) {
                return enlarge(newSize);
            } else if (newSize < size) {
                return shrink(newSize);
            } else {
                return this;
            }
        }

        @Override
        public int size() {
            return size;
        }

        @Override
        public T doGet(final int i) {
            int idx = i / chunkSize;
            return children[idx].doGet(i % chunkSize);
        }

        @Override
        protected TreeVector<T> doSet(int i, T t) {
            final int childIndex = i / chunkSize;
            TreeVector<T> newChild = children[childIndex].doSet(i % chunkSize, t);
            InternalNode<T> newNode = new InternalNode<T>(size, chunkSize, children);
            newNode.update(childIndex, newChild);
            return newNode;
        }

        private void update(int childIndex, TreeVector<T> newChild) {
            children[childIndex] = newChild;
        }

        private TreeVector<T> enlarge(int newSize) {
            int newChunkSize = getChunkSize(newSize);
            int newChildNum = getChildNum(newSize, newChunkSize);
            TreeVector<T>[] newChildren = Arrays.copyOf(children, BLOCK_SIZE);

            if (newChunkSize > chunkSize) {
                newChildren[0] = enlarge(newChunkSize);
                for (int i = 1; i < newChildNum; i++) {
                    newChildren[i] = createChild(newChunkSize);
                }
            } else {
                for (int i = getChildNum(size, chunkSize); i < newChildNum; i++) {
                    newChildren[i] = createChild(chunkSize);
                }
            }
            return new InternalNode<T>(newSize, newChunkSize, newChildren);
        }

        protected TreeVector<T> shrink(int newSize) {
            int newChunkSize = getChunkSize(newSize);
            InternalNode<T> actualNode = this;

            while (newChunkSize < actualNode.chunkSize) {
                actualNode = (InternalNode<T>) actualNode.children[0];
            }
            return new InternalNode<T>(newSize, newChunkSize, actualNode.children);
        }

        public boolean isEmpty() {
            return size == 0;
        }

        private LazyList<T> getEntrySeqFrom(final int index) {
            if (index >= getChildNum(size, chunkSize)) {
                return LazyList.emptyStream();
            } else {
                return children[index].getEntrySeq().append(new Memoize<LazyList<T>>() {
                    @Override
                    protected LazyList<T> call() {
                        return getEntrySeqFrom(index + 1);
                    }
                });
            }
        }

        @Override
        protected LazyList<T> getEntrySeq() {
            return getEntrySeqFrom(0);
        }

    }

    private static class Leaf<T> extends TreeVector<T> {
        private final Object[] values;

        public Leaf() {
            this.values = new Object[BLOCK_SIZE];
        }

        private Leaf(Object[] values) {
            this.values = Arrays.copyOf(values, BLOCK_SIZE);
        }

        @Override
        public int size() {
            throw new UnsupportedOperationException();
        }

        @Override
        public T doGet(int i) {
            return (T) values[i];
        }

        public Leaf<T> doSet(int i, T t) {
            Object newValues[] = Arrays.copyOf(values, BLOCK_SIZE);
            newValues[i] = t;
            Leaf<T> newLeaf = new Leaf<T>(newValues);
            return newLeaf;
        }

        public TreeVector<T> resize(int newSize) {
            throw new UnsupportedOperationException();
        }

        public boolean isEmpty() {
            throw new UnsupportedOperationException();
        }

        @Override
        protected LazyList<T> getEntrySeq() {
            return (LazyList<T>) LazyList.fromArray(values);
        }

    }
}
