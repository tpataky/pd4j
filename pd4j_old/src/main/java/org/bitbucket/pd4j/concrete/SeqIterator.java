/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.pd4j.concrete;

import java.util.Iterator;
import org.bitbucket.pd4j.concept.Seq;

/**
 *
 * @author pataky
 */
class SeqIterator<T> implements Iterator<T> {

    private Seq<T, ?> seq;

    public SeqIterator(Seq<T, ?> seq) {
        this.seq = seq;
    }

    public boolean hasNext() {
        return !seq.isEmpty();
    }

    public T next() {
        T head = seq.head();
        seq = seq.tail();
        return head;
    }

    public void remove() {
        // Seriously?!
        throw new UnsupportedOperationException();
    }

}
