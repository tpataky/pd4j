/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.pd4j.concrete;

/**
 *
 * @author pataky
 */
class FingerTreeUtils {

    static abstract class Node<T> {
    }

    static class Node2<T> extends Node<T> {
        public final T left;
        public final T right;

        public Node2(T left, T right) {
            this.left = left;
            this.right = right;
        }
    }

    static class Node3<T> extends Node<T> {
        public final T left;
        public final T mid;
        public final T right;

        public Node3(T left, T mid, T right) {
            this.left = left;
            this.mid = mid;
            this.right = right;
        }
    }

    static interface Measurable<V, T> {
        V measure();
        T value();
    }

    static class Measured<T> implements Measurable<T, T> {
        private final T value;

        public Measured(T value) {
            this.value = value;
        }

        public T measure() {
            return value;
        }

        public T value() {
            return value;
        }

        @Override
        public String toString() {
            return value.toString();
        }

    }

    static abstract class MeasuredNode<V, T> implements Measurable<V, MeasuredNode<V, T>> {
        public final V max;

        public MeasuredNode(V max) {
            this.max = max;
        }

        public V measure() {
            return max;
        }

        public MeasuredNode<V, T> value() {
            return this;
        }

        abstract public Object[] toList();
    }

    static class MeasuredNode2<V, T> extends MeasuredNode<V, T> {
        public final T left;
        public final T right;

        public MeasuredNode2(V max, T left, T right) {
            super(max);
            this.left = left;
            this.right = right;
        }

        @Override
        public Object[] toList() {
            return new Object[] { left, right };
        }

        @Override
        public String toString() {
            return "(" + left + ", " + right + ')';
        }
    }

    static class MeasuredNode3<V, T> extends MeasuredNode<V, T> {
        public final T left;
        public final T mid;
        public final T right;

        public MeasuredNode3(V max, T left, T mid, T right) {
            super(max);
            this.left = left;
            this.mid = mid;
            this.right = right;
        }

        @Override
        public Object[] toList() {
            return new Object[] { left, mid, right };
        }

        @Override
        public String toString() {
            return "(" + left + ", " + mid + ", " + right + ')';
        }

    }

}
