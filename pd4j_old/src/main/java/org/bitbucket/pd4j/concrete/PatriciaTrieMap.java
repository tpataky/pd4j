/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.pd4j.concrete;

import org.bitbucket.pd4j.concept.HashMap;
import org.bitbucket.pd4j.concept.Seq;
import org.bitbucket.pd4j.util.Option;
import org.bitbucket.pd4j.util.Pair;
import java.util.Arrays;
import java.util.Iterator;
import org.bitbucket.pd4j.util.Memoize;

/**
 *
 * @author pataky
 */
public abstract class PatriciaTrieMap<K, V> implements HashMap<K, V, PatriciaTrieMap<K, V>>, Iterable<Pair<K, V>> {

    private static Empty emptyInstance = new Empty();

    public static <K, V> PatriciaTrieMap<K, V> emptyMap() {
        return emptyInstance;
    }

    public PatriciaTrieMap<K, V> empty() {
        return emptyInstance;
    }

    public Option<V> get(K key) {
        return get(key.hashCode(), key);
    }

    protected abstract Option<V> get(int hash, K key);

    public PatriciaTrieMap<K, V> put(K key, V value) {
        return put(key.hashCode(), key, value);
    }

    protected abstract PatriciaTrieMap<K, V> put(int hash, K key, V value);

    public final Seq<Pair<K, V>, ?> entrySeq() {
        return getEntrySeq();
    }

    protected abstract LazyList<Pair<K, V>> getEntrySeq();

    public Iterator<Pair<K, V>> iterator() {
        return new SeqIterator<Pair<K, V>>(getEntrySeq());
    }

    static class Empty<K, V> extends PatriciaTrieMap<K, V> {

        @Override
        protected PatriciaTrieMap<K, V> put(int hash, K key, V value) {
            return new Bucket<K, V>(hash, key, value);
        }

        public Option<V> get(int hash, K key) {
            return Option.nothing();
        }

        public PatriciaTrieMap<K, V> remove(K key) {
            return this;
        }

        @Override
        protected LazyList<Pair<K, V>> getEntrySeq() {
            return LazyList.emptyStream();
        }

    }

    static class Node<K, V> extends PatriciaTrieMap<K, V> {
        private final int bits;
        private final int depth;
        private final PatriciaTrieMap<K, V> left;
        private final PatriciaTrieMap<K, V> right;

        public Node(int bits, int depth, PatriciaTrieMap<K, V> left, PatriciaTrieMap<K, V> right) {
            this.bits = bits;
            this.depth = depth;
            this.left = left;
            this.right = right;
        }

        @Override
        protected PatriciaTrieMap<K, V> put(int hash, K key, V value) {
            int position = 1;
            for (int i = 0; i < depth; i++) {
                int hp = hash & position;
                int bp = bits & position;
                if (hp != bp) {
                    if (hp == 0) {
                        return new Node<K, V>(bits, i, new Bucket<K, V>(hash, key, value), this);
                    } else {
                        return new Node<K, V>(bits, i, this, new Bucket<K, V>(hash, key, value));
                    }
                }
                position = position << 1;
            }

            if ((hash & position) == 0) {
                PatriciaTrieMap<K, V> newLeft = left.put(hash, key, value);
                return new Node<K, V>(bits, depth, newLeft, right);
            } else {
                PatriciaTrieMap<K, V> newRight = right.put(hash, key, value);
                return new Node<K, V>(bits, depth, left, newRight);
            }
        }

        public Option<V> get(int hash, K key) {
            int position = 1;
            for (int i = 0; i < depth; i++) {
                int hp = hash & position;
                int bp = bits & position;
                if (hp != bp) {
                    return Option.nothing();
                }
                position = position << 1;
            }

            if ((hash & position) == 0) {
                return left.get(hash, key);
            } else {
                return right.get(hash, key);
            }
        }

        public PatriciaTrieMap<K, V> remove(K key) {
            int hash = key.hashCode();

            int position = 1;
            for (int i = 0; i < depth; i++) {
                int hp = hash & position;
                int bp = bits & position;
                if (hp != bp) {
                    return this;
                }
                position = position << 1;
            }

            if ((hash & position) == 0) {
                PatriciaTrieMap<K, V> newLeft = left.remove(key);
                if (newLeft == emptyInstance) {
                    return right;
                } else {
                    return new Node<K, V>(bits, depth, newLeft, right);
                }
            } else {
                PatriciaTrieMap<K, V> newRight = right.remove(key);
                if (newRight == emptyInstance) {
                    return left;
                } else {
                    return new Node<K, V>(bits, depth, left, newRight);
                }
            }
        }

        @Override
        protected LazyList<Pair<K, V>> getEntrySeq() {
            return left.getEntrySeq().append(new Memoize<LazyList<Pair<K, V>>>() {
                @Override
                protected LazyList<Pair<K, V>> call() {
                    return right.getEntrySeq();
                }
            });
        }

    }

    static class Bucket<K, V> extends PatriciaTrieMap<K, V> {
        private final int bits;

        private final Object[] keys;
        private final Object[] values;

        public Bucket(int hash, K key, V value) {
            bits = hash;
            keys = new Object[1];
            values = new Object[1];
            keys[0] = key;
            values[0] = value;
        }

        public Bucket(int bits, Object[] keys, Object[] values) {
            this.bits = bits;
            this.keys = keys;
            this.values = values;
        }

        public PatriciaTrieMap<K, V> put(int hash, K key, V value) {
            if (bits == hash) {
                return insertIntoThis(key, value);
            } else {
                return insertIntoNewNode(hash, key, value);
            }
        }

        private PatriciaTrieMap<K, V> insertIntoNewNode(int hash, K key, V value) {
            int position = 1;
            for (int i = 0; i < 32; i++) {
                int hp = hash & position;
                int bp = bits & position;
                if (hp != bp) {
                    if (hp == 0) {
                        return new Node<K, V>(bits, i, new Bucket<K, V>(hash, key, value), this);
                    } else {
                        return new Node<K, V>(bits, i, this, new Bucket<K, V>(hash, key, value));
                    }
                }
                position = position << 1;
            }
            throw new IllegalStateException();
        }

        private PatriciaTrieMap<K, V> insertIntoThis(K key, V value) {
            final int index = getIndex(key);
            if (index < keys.length) {
                return put(index, value);
            }

            final int newLength = keys.length + 1;
            final Object[] newKeys = Arrays.copyOf(keys, newLength);
            final Object[] newValues = Arrays.copyOf(values, newLength);

            newKeys[newLength - 1] = key;
            newValues[newLength - 1] = value;

            return new Bucket<K, V>(bits, newKeys, newValues);
        }

        private int getIndex(K key) {
            for (int i = 0; i < keys.length; i++) {
                if (key.equals(keys[i])) {
                    return i;
                }
            }
            return keys.length;
        }

        private Bucket<K, V> put(int index, V value) {
            final Object[] newValues = Arrays.copyOf(values, values.length);
            newValues[index] = value;
            return new Bucket<K, V>(bits, keys, newValues);
        }

        public Option<V> get(int hash, K key) {
            int index = getIndex(key);
            if (index < keys.length) {
                return Option.some((V) values[index]);
            }
            return Option.nothing();
        }

        @Override
        public PatriciaTrieMap<K, V> remove(K key) {
            int index = getIndex(key);
            if (index >= keys.length) {
                return this;
            }
            if (keys.length == 1) {
                return emptyInstance;
            }
            final Object[] newKeys = removeElement(keys, index);
            final Object[] newValues = removeElement(values, index);

            return new Bucket<K, V>(bits, newKeys, newValues);
        }

        static Object[] removeElement(Object[] original, int element) {
            Object[] newArray = new Object[original.length - 1];
            System.arraycopy(original, 0, newArray, 0, element);
            System.arraycopy(original, element + 1, newArray, element, newArray.length - element);
            return newArray;
        }

        protected LazyList<Pair<K, V>> getEntrySeq() {
            return getSeqFrom(0);
        }

        private LazyList<Pair<K, V>> getSeqFrom(final int i) {
            if (i == values.length) {
                return LazyList.<Pair<K, V>>emptyStream();
            } else {

                return LazyList.cons(Pair.create((K) keys[i], (V) values[i]),

                    new Memoize<LazyList<Pair<K, V>>>() {
                        @Override
                        protected LazyList<Pair<K, V>> call() {
                            return getSeqFrom(i + 1);
                        }
                    });
            }
        }

    }

}
