/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.pd4j.concrete;

import java.util.Iterator;
import org.bitbucket.pd4j.concept.Deque;
import java.util.NoSuchElementException;
import static org.bitbucket.pd4j.concrete.FingerTreeUtils.*;
import static org.bitbucket.pd4j.concrete.ArrayUtils.*;

/**
 *
 * @author pataky
 */
public abstract class FingerTreeQueue<T> implements Deque<T, FingerTreeQueue<T>>, Iterable<T> {

    public static <T> FingerTreeQueue<T> emptyQueue() {
        return empty;
    }

    @Override
    public FingerTreeQueue<T> empty() {
        return empty;
    }

    public abstract FingerTreeQueue<T> append(FingerTreeQueue<T> other);

    private final static Empty empty = new Empty();

    public Iterator<T> iterator() {
        return new SeqIterator<T>(this);
    }

    static class Empty<T> extends FingerTreeQueue<T> {
        @Override
        public FingerTreeQueue<T> cons(T t) {
            return new Single<T>(t);
        }

        @Override
        public T head() {
            throw new NoSuchElementException();
        }

        @Override
        public FingerTreeQueue<T> tail() {
            throw new NoSuchElementException();
        }

        @Override
        public FingerTreeQueue<T> snoc(T t) {
            return new Single<T>(t);
        }

        @Override
        public FingerTreeQueue<T> init() {
            throw new NoSuchElementException();
        }

        @Override
        public T last() {
            throw new NoSuchElementException();
        }

        @Override
        public boolean isEmpty() {
            return true;
        }

        @Override
        public FingerTreeQueue<T> append(FingerTreeQueue<T> other) {
            return other;
        }

    }

    static class Single<T> extends FingerTreeQueue<T> {
        final T elem;

        public Single(T elem) {
            this.elem = elem;
        }

        @Override
        public FingerTreeQueue<T> cons(T t) {
            return new Deep<T>(array(t), empty, array(this.elem));
        }

        @Override
        public T head() {
            return elem;
        }

        @Override
        public FingerTreeQueue<T> tail() {
            return empty;
        }

        @Override
        public FingerTreeQueue<T> snoc(T t) {
            return new Deep<T>(array(this.elem), empty, array(t));
        }

        @Override
        public FingerTreeQueue<T> init() {
            return empty;
        }

        @Override
        public T last() {
            return elem;
        }

        @Override
        public boolean isEmpty() {
            return false;
        }

        @Override
        public FingerTreeQueue<T> append(FingerTreeQueue<T> other) {
            return other.cons(elem);
        }

    }

    static class Deep<T> extends FingerTreeQueue<T> {
        final Object[] left;
        final FingerTreeQueue<Node<T>> tree;
        final Object[] right;

        public Deep(Object[] left, FingerTreeQueue<Node<T>> tree, Object[] right) {
            this.left = left;
            this.tree = tree;
            this.right = right;
        }

        @Override
        public FingerTreeQueue<T> cons(T t) {
            if (left.length == 4) {
                return new Deep<T>(array(t, left[0]), tree.cons(new Node3<T>((T) left[1], (T) left[2], (T) left[3])), right);
            } else {
                return new Deep<T>(appL(t, left), tree, right);
            }
        }

        @Override
        public T head() {
            return (T) left[0];
        }

        @Override
        public FingerTreeQueue<T> tail() {
            if (left.length > 1) {
                return new Deep<T>(popL(left), tree, right);
            }

            if (tree instanceof Empty) {
                if (right.length == 1) {
                    return new Single<T>((T) right[0]);
                } else {
                    return new Deep<T>(array(right[0]), tree, popL(right));
                }
            } else {
                Node<T> n = tree.head();
                if (n instanceof Node2) {
                    Node2 n2 = (Node2) n;
                    return new Deep<T>(array(n2.left, n2.right), tree.tail(), right);
                } else {
                    Node3 n3 = (Node3) n;
                    return new Deep<T>(array(n3.left, n3.mid, n3.right), tree.tail(), right);
                }
            }
        }

        @Override
        public FingerTreeQueue<T> snoc(T t) {
            if (right.length == 4) {
                return new Deep<T>(left, tree.snoc(new Node3<T>((T) right[0], (T) right[1], (T) right[2])), array(right[3], t));
            } else {
                return new Deep<T>(left, tree, appR(right, t));
            }
        }

        @Override
        public FingerTreeQueue<T> init() {
            if (right.length > 1) {
                return new Deep<T>(left, tree, popR(right));
            }

            if (tree instanceof Empty) {
                if (left.length == 1) {
                    return new Single<T>((T) left[0]);
                } else {
                    return new Deep<T>(popR(left), tree, array(left[left.length - 1]));
                }
            } else {
                Node<T> n = tree.last();
                if (n instanceof Node2) {
                    Node2 n2 = (Node2) n;
                    return new Deep<T>(left, tree.init(), array(n2.left, n2.right));
                } else {
                    Node3 n3 = (Node3) n;
                    return new Deep<T>(left, tree.init(), array(n3.left, n3.mid, n3.right));
                }
            }
        }

        @Override
        public T last() {
            return (T) right[right.length - 1];
        }

        @Override
        public boolean isEmpty() {
            return true;
        }

        @Override
        public FingerTreeQueue<T> append(FingerTreeQueue<T> other) {
            if (other instanceof Empty) {
                return this;
            }
            if (other instanceof Single) {
                Single<T> s = (Single<T>) other;
                return snoc(s.elem);
            }
            Deep<T> d = (Deep<T>) other;
            Object[] app = app(right, d.left);
            switch (app.length) {
                case 2:
                    return new Deep<T>(left, tree.snoc(new Node2<T>((T) app[0], (T) app[1])).append(d.tree), right);
                case 3:
                    return new Deep<T>(left, tree.snoc(new Node3<T>((T) app[0], (T) app[1], (T) app[2])).append(d.tree), right);
                case 4:
                    return new Deep<T>(left, tree
                            .snoc(new Node2<T>((T) app[0], (T) app[1]))
                            .snoc(new Node2<T>((T) app[2], (T) app[3]))
                            .append(d.tree), right);
                case 5:
                    return new Deep<T>(left, tree
                            .snoc(new Node3<T>((T) app[0], (T) app[1], (T) app[2]))
                            .snoc(new Node2<T>((T) app[3], (T) app[4]))
                            .append(d.tree), right);
                case 6:
                    return new Deep<T>(left, tree
                            .snoc(new Node3<T>((T) app[0], (T) app[1], (T) app[2]))
                            .snoc(new Node3<T>((T) app[3], (T) app[4], (T) app[5]))
                            .append(d.tree), right);
                case 7:
                    return new Deep<T>(left, tree
                            .snoc(new Node3<T>((T) app[0], (T) app[1], (T) app[2]))
                            .snoc(new Node2<T>((T) app[3], (T) app[4]))
                            .snoc(new Node2<T>((T) app[5], (T) app[6]))
                            .append(d.tree), right);
                case 8:
                    return new Deep<T>(left, tree
                            .snoc(new Node3<T>((T) app[0], (T) app[1], (T) app[2]))
                            .snoc(new Node3<T>((T) app[3], (T) app[4], (T) app[5]))
                            .snoc(new Node2<T>((T) app[6], (T) app[7]))
                            .append(d.tree), right);
            }
            throw new IllegalStateException();
        }

    }

}
