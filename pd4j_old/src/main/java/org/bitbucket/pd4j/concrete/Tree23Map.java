package org.bitbucket.pd4j.concrete;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Map;

import org.bitbucket.pd4j.concept.OrderedMap;
import org.bitbucket.pd4j.util.Option;
import org.bitbucket.pd4j.util.Pair;

public abstract class Tree23Map<K, V, Ord extends Comparator<K>> implements
        OrderedMap<K, V, Ord, Tree23Map<K, V, Ord>>,
        Iterable<Pair<K, V>>
{

    public static <K extends Comparable<K>, V> Tree23Map<K, V, NatOrd<K>> emptyMap() {
        return new Empty<K, V, NatOrd<K>>(NatOrd.<K>getInstance());
    }

    public static <K, V, Ord extends Comparator<K>> Tree23Map<K, V, Ord> emptyMap(Ord ord) {
        return new Empty(ord);
    }

    public abstract Option<V> get(K elem);

    public abstract Tree23Map<K, V, Ord> put(K key, V value);

    public abstract Tree23Map<K, V, Ord> remove(K elem);

    public abstract boolean isEmpty();

    abstract Tree23Map<K, V, Ord> removeMin();

    protected abstract Pair<K, V> getMin();

    static class Empty<K, V, Ord extends Comparator<K>> extends Tree23Map<K, V, Ord> {
        final Ord ord;

        public Empty(Ord ord) {
            this.ord = ord;
        }

        public Tree23Map<K, V, Ord> empty() {
            return this;
        }

        @Override
        public Tree23Map<K, V, Ord> put(K key, V value) {
            return new Node2<K, V, Ord>(this, key, value);
        }

        @Override
        public boolean isEmpty() {
            return true;
        }

        @Override
        public Option<V> get(K elem) {
            return Option.nothing();
        }

        @Override
        Tree23Map<K, V, Ord> removeMin() {
            throw new UnsupportedOperationException();
        }

        @Override
        public Tree23Map<K, V, Ord> remove(K elem) {
            throw new UnsupportedOperationException();
        }

        @Override
        protected Pair<K, V> getMin() {
            throw new UnsupportedOperationException();
        }

    }

    static class Node2<K, V, Ord extends Comparator<K>> extends Tree23Map<K, V, Ord> {
        final Empty<K, V, Ord> empty;
        final K key;
        final V value;
        final Tree23Map<K, V, Ord> left;
        final Tree23Map<K, V, Ord> right;

        public Node2(Empty<K, V, Ord> empty, K key, V value) {
            this.empty = empty;
            this.key = key;
            this.value = value;
            this.left = empty;
            this.right = empty;
        }

        public Node2(Empty<K, V, Ord> empty, K key, V value, Tree23Map<K, V, Ord> left, Tree23Map<K, V, Ord> right) {
            this.empty = empty;
            this.key = key;
            this.value = value;
            this.left = left;
            this.right = right;
        }

        public Tree23Map<K, V, Ord> empty() {
            return empty;
        }

        @Override
        public Tree23Map<K, V, Ord> put(K key, V value) {
            final int cmp = empty.ord.compare(key, this.key);
            if (cmp < 0) {
                return insertLeft(key, value);
            } else if (cmp > 0) {
                return insertRight(key, value);
            } else {
                return new Node2<K, V, Ord>(empty, key, value, left, right);
            }
        }

        private Tree23Map<K, V, Ord> balanceL(Tree23Map<K, V, Ord> newLeft, K key, V value) {
            if (newLeft instanceof Empty || left instanceof Node2 && newLeft instanceof Node3) {
                if (right instanceof Node2) {
                    Node2<K, V, Ord> n = (Node2<K, V, Ord>) right;
                    return new Node3<K, V, Ord>(empty, newLeft, key, value, n.left, n.key, n.value, n.right);
                } else {
                    Node3<K, V, Ord> n = (Node3<K, V, Ord>) right;
                    return new Node2<K, V, Ord>(empty, n.lKey, n.lValue,
                            new Node2<K, V, Ord>(empty, key, value, newLeft, n.left),
                            new Node2<K, V, Ord>(empty, n.rKey, n.rValue, n.mid, n.right));
                }
            }
            return new Node2<K, V, Ord>(empty, key, value, newLeft, right);
        }

        private Tree23Map<K, V, Ord> balanceR(Tree23Map<K, V, Ord> newRight, K key, V value) {
            if (newRight instanceof Empty || right instanceof Node2 && newRight instanceof Node3) {
                if (left instanceof Node2) {
                    Node2<K, V, Ord> n = (Node2<K, V, Ord>) left;
                    return new Node3<K, V, Ord>(empty, n.left, n.key, n.value, n.right, key, value, newRight);
                } else {
                    Node3<K, V, Ord> n = (Node3<K, V, Ord>) left;
                    return new Node2<K, V, Ord>(empty, n.rKey, n.rValue,
                            new Node2<K, V, Ord>(empty, n.lKey, n.lValue, n.left, n.mid),
                            new Node2<K, V, Ord>(empty, key, value, n.right, newRight));
                }
            }
            return new Node2<K, V, Ord>(empty, key, value, left, newRight);
        }

        private Tree23Map<K, V, Ord> insertRight(K key, V value) {
            if (right instanceof Empty) {
                return new Node3<K, V, Ord>(empty, this.key, this.value, key, value);
            }

            Tree23Map<K, V, Ord> newRight = right.put(key, value);
            if (right instanceof Node3 && newRight instanceof Node2) {
                Node2<K, V, Ord> r = (Node2<K, V, Ord>) newRight;
                return new Node3<K, V, Ord>(empty, left, this.key, this.value, r.left, r.key, r.value, r.right);
            }

            return new Node2<K, V, Ord>(empty, this.key, this.value, left, newRight);
        }

        private Tree23Map<K, V, Ord> insertLeft(K key, V value) {
            if (left instanceof Empty) {
                return new Node3<K, V, Ord>(empty, key, value, this.key, this.value);
            }

            Tree23Map<K, V, Ord> newLeft = left.put(key, value);
            if (left instanceof Node3 && newLeft instanceof Node2) {
                Node2<K, V, Ord> l = (Node2<K, V, Ord>) newLeft;
                return new Node3<K, V, Ord>(empty, l.left, l.key, l.value, l.right, this.key, this.value, right);
            }

            return new Node2<K, V, Ord>(empty, this.key, this.value, newLeft, right);
        }

        @Override
        public boolean isEmpty() {
            return false;
        }

        @Override
        public Option<V> get(K key) {
            final int cmp = empty.ord.compare(key, this.key);
            if (cmp == 0) {
                return Option.some(value);
            } else if (cmp < 0) {
                return left.get(key);
            } else {
                return right.get(key);
            }
        }

        @Override
        public Tree23Map<K, V, Ord> remove(K key) {
            final int cmp = empty.ord.compare(key, this.key);
            if (cmp == 0) {
                if (left instanceof Empty) {
                    return empty;
                } else {
                    Pair<K, V> min = right.getMin();
                    Tree23Map<K, V, Ord> newRight = right.removeMin();
                    return balanceR(newRight, min.first, min.second);
                }
            } else if (cmp < 0) {
                return balanceL(left.remove(key), this.key, this.value);
            } else {
                return balanceR(right.remove(key), this.key, this.value);
            }
        }

        protected Pair<K, V> getMin() {
            if (left instanceof Empty) {
                return Pair.create(key, value);
            } else {
                return left.getMin();
            }
        }

        @Override
        Tree23Map<K, V, Ord> removeMin() {
            if (left instanceof Empty) {
                return empty;
            }

            Tree23Map<K, V, Ord> newLeft = left.removeMin();
            return balanceL(newLeft, this.key, this.value);
        }

    }

    static class Node3<K, V, Ord extends Comparator<K>> extends Tree23Map<K, V, Ord> {
        final Empty<K, V, Ord> empty;
        final Tree23Map<K, V, Ord> left;
        final K lKey;
        final V lValue;
        final Tree23Map<K, V, Ord> mid;
        final K rKey;
        final V rValue;
        final Tree23Map<K, V, Ord> right;

        public Node3(Empty<K, V, Ord> empty, K lElem, V lValue, K rElem, V rValue) {
            this.empty = empty;
            this.lKey = lElem;
            this.lValue = lValue;
            this.rKey = rElem;
            this.rValue = rValue;
            this.left = empty;
            this.mid = empty;
            this.right = empty;
        }

        public Node3(Empty<K, V, Ord> empty, Tree23Map<K, V, Ord> left, K lElem, V lValue, Tree23Map<K, V, Ord> mid, K rElem, V rValue, Tree23Map<K, V, Ord> right) {
            this.empty = empty;
            this.lKey = lElem;
            this.lValue = lValue;
            this.rKey = rElem;
            this.rValue = rValue;
            this.left = left;
            this.mid = mid;
            this.right = right;
        }

        public Tree23Map<K, V, Ord> empty() {
            return empty;
        }

        @Override
        public Tree23Map<K, V, Ord> put(K key, V value) {
            final int leftCmp = empty.ord.compare(key, this.lKey);
            if (leftCmp < 0) {
                return insertLeft(key, value);
            } else if (leftCmp == 0) {
                return new Node3<K, V, Ord>(empty, left, key, value, mid, rKey, rValue, right);
            } else {
                final int rightCmp = empty.ord.compare(key, this.rKey);
                if (rightCmp < 0) {
                    return insertMid(key, value);
                } else if (rightCmp > 0) {
                    return insertRight(key, value);
                } else {
                    return new Node3<K, V, Ord>(empty, left, lKey, lValue, mid, key, value, right);
                }
            }
        }

        private Tree23Map<K, V, Ord> insertRight(K key, V value) {
            if (right instanceof Empty) {
                return new Node2<K, V, Ord>(empty, rKey, rValue,
                        new Node2<K, V, Ord>(empty, lKey, lValue),
                        new Node2<K, V, Ord>(empty, key, value));
            }

            Tree23Map<K, V, Ord> newRight = right.put(key, value);
            if (right instanceof Node3 && newRight instanceof Node2) {
                return new Node2<K, V, Ord>(empty, rKey, rValue,
                        new Node2<K, V, Ord>(empty, lKey, lValue, left, mid),
                        newRight);
            }

            return new Node3<K, V, Ord>(empty, left, lKey, lValue, mid, rKey, rValue, newRight);
        }

        private Tree23Map<K, V, Ord> insertMid(K key, V value) {
            Tree23Map<K, V, Ord> newMid = mid.put(key, value);
            if (mid instanceof Node3 && newMid instanceof Node2 || mid instanceof Empty) {
                Node2<K, V, Ord> m = (Node2<K, V, Ord>) newMid;
                return new Node2<K, V, Ord>(empty, m.key, m.value,
                        new Node2<K, V, Ord>(empty, lKey, lValue, left, m.left),
                        new Node2<K, V, Ord>(empty, rKey, rValue, m.right, right));
            }

            return new Node3<K, V, Ord>(empty, left, lKey, lValue, newMid, rKey, rValue, right);
        }

        private Tree23Map<K, V, Ord> insertLeft(K key, V value) {
            if (left instanceof Empty) {
                return new Node2<K, V, Ord>(empty, lKey, lValue,
                        new Node2<K, V, Ord>(empty, key, value),
                        new Node2<K, V, Ord>(empty, rKey, rValue));
            }

            Tree23Map<K, V, Ord> newLeft = left.put(key, value);
            if (left instanceof Node3 && newLeft instanceof Node2) {
                return new Node2<K, V, Ord>(empty, lKey, lValue,
                        newLeft,
                        new Node2<K, V, Ord>(empty, rKey, rValue, mid, right));
            }

            return new Node3<K, V, Ord>(empty, newLeft, lKey, lValue, mid, rKey, rValue, right);
        }

        @Override
        public boolean isEmpty() {
            return false;
        }

        @Override
        public Option<V> get(K key) {
            final int leftCmp = empty.ord.compare(key, this.lKey);
            if (leftCmp < 0) {
                return left.get(key);
            } else if (leftCmp == 0) {
                return Option.some(lValue);
            } else {
                final int rightCmp = empty.ord.compare(key, this.rKey);
                if (rightCmp < 0) {
                    return mid.get(key);
                } else if (rightCmp > 0) {
                    return right.get(key);
                } else {
                    return Option.some(rValue);
                }
            }
        }

        @Override
        Tree23Map<K, V, Ord> removeMin() {
            if (left instanceof Empty) {
                return new Node2<K, V, Ord>(empty, rKey, rValue);
            }

            Tree23Map<K, V, Ord> newLeft = left.removeMin();
            return balanceL(newLeft);
        }

        @Override
        public Tree23Map<K, V, Ord> remove(K key) {
            final int leftCmp = empty.ord.compare(key, this.lKey);
            if (leftCmp < 0) {
                return balanceL(left.remove(key));
            } else if (leftCmp == 0) {
                if (right instanceof Empty) {
                    return new Node2<K, V, Ord>(empty, rKey, rValue);
                } else {
                    Pair<K, V> min = mid.getMin();
                    Tree23Map<K, V, Ord> newMid = mid.removeMin();
                    return balanceM(newMid, min.first, min.second);
                }
            } else {
                final int rightCmp = empty.ord.compare(key, this.rKey);
                if (rightCmp < 0) {
                    Tree23Map<K, V, Ord> newMid = mid.remove(key);
                    return balanceM(newMid, lKey, lValue);
                } else if (rightCmp > 0) {
                    Tree23Map<K, V, Ord> newRight = right.remove(key);
                    return balanceR(newRight, rKey, rValue);
                } else {
                    if (right instanceof Empty) {
                        return new Node2<K, V, Ord>(empty, lKey, lValue);
                    } else {
                        Pair<K, V> min = right.getMin();
                        Tree23Map<K, V, Ord> newRight = right.removeMin();
                        return balanceR(newRight, min.first, min.second);
                    }
                }
            }
        }

        private Tree23Map<K, V, Ord> balanceL(Tree23Map<K, V, Ord> newLeft) {
            if (newLeft instanceof Empty || left instanceof Node2 && newLeft instanceof Node3) {
                if (mid instanceof Node2) {
                    Node2<K, V, Ord> n = (Node2<K, V, Ord>) mid;
                    return new Node2<K, V, Ord>(empty,
                            rKey, rValue,
                            new Node3<K, V, Ord>(empty, newLeft, lKey, lValue, n.left, n.key, n.value, n.right),
                            right);
                } else {
                    Node3<K, V, Ord> n = (Node3<K, V, Ord>) mid;
                    return new Node3<K, V, Ord>(empty,
                            new Node2<K, V, Ord>(empty, lKey, lValue, newLeft, n.left),
                            n.lKey, n.lValue,
                            new Node2<K, V, Ord>(empty, n.rKey, n.lValue, n.mid, n.right),
                            rKey, rValue,
                            right);
                }
            } else {
                return new Node3<K, V, Ord>(empty, newLeft, lKey, lValue, mid, rKey, rValue, right);
            }
        }

        private Tree23Map<K, V, Ord> balanceR(Tree23Map<K, V, Ord> newRight, K key, V value) {
            if (newRight instanceof Empty || right instanceof Node2 && newRight instanceof Node3) {
                if (mid instanceof Node2) {
                    Node2<K, V, Ord> n = (Node2<K, V, Ord>) mid;
                    return new Node2<K, V, Ord>(empty,
                            lKey, lValue,
                            left,
                            new Node3<K, V, Ord>(empty, n.left, n.key, n.value, n.right, key, value, newRight));
                } else {
                    Node3<K, V, Ord> n = (Node3<K, V, Ord>) mid;
                    return new Node3<K, V, Ord>(empty,
                            left,
                            lKey, lValue,
                            new Node2<K, V, Ord>(empty, n.lKey, n.lValue, n.left, n.right),
                            n.rKey, n.rValue,
                            new Node2<K, V, Ord>(empty, key, value, n.right, newRight));
                }
            } else {
                return new Node3<K, V, Ord>(empty, left, lKey, lValue, mid, key, value, newRight);
            }
        }

        private Tree23Map<K, V, Ord> balanceM(Tree23Map<K, V, Ord> newMid, K key, V value) {
            if (newMid instanceof Empty || mid instanceof Node2 && newMid instanceof Node3) {
                if (left instanceof Node2) {
                    Node2<K, V, Ord> n = (Node2<K, V, Ord>) left;
                    return new Node2<K, V, Ord>(empty, rKey, rValue,
                            new Node3<K, V, Ord>(empty, n.left, n.key, n.value, n.right, key, value, newMid),
                            right);
                } else {
                    Node3<K, V, Ord> n = (Node3<K, V, Ord>) left;
                    return new Node3<K, V, Ord>(empty,
                            new Node2<K, V, Ord>(empty, n.lKey, n.lValue, n.left, n.mid),
                            n.rKey, n.rValue,
                            new Node2<K, V, Ord>(empty, key, value, n.right, newMid),
                            rKey, rValue,
                            right);
                }
            } else {
                return new Node3<K, V, Ord>(empty, left, key, value, newMid, rKey, rValue, right);
            }
        }

        @Override
        protected Pair<K, V> getMin() {
            if (left instanceof Empty) {
                return Pair.create(lKey, lValue);
            } else {
                return left.getMin();
            }
        }

    }

    public Iterator<Pair<K, V>> iterator() {
        return null;
    }

}
