/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.pd4j.concrete;

import java.util.Iterator;
import org.bitbucket.pd4j.concept.PriorityQueue;

/**
 *
 * @author pataky
 */
class PriorityQueueIterator<T> implements Iterator<T> {
    private PriorityQueue<T, ?, ?> queue;

    public PriorityQueueIterator(PriorityQueue<T, ?, ?> queue) {
        this.queue = queue;
    }

    public boolean hasNext() {
        return !queue.isEmpty();
    }

    public T next() {
        T peek = queue.peek();
        queue = queue.pop();
        return peek;
    }

    public void remove() {
        throw new UnsupportedOperationException();
    }

}
