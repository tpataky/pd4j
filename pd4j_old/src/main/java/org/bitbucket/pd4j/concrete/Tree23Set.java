/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.pd4j.concrete;

import java.util.Iterator;
import org.bitbucket.pd4j.concept.OrderedSet;
import java.util.Comparator;

import org.bitbucket.pd4j.util.Memoize;

/**
 *
 * @author pataky
 */
public abstract class Tree23Set<T, Ord extends Comparator<T>> implements
        OrderedSet<T, Ord, Tree23Set<T, Ord>>,
        Iterable<T>
{

    public static <K extends Comparable<K>> Tree23Set<K, NatOrd<K>> emptySet() {
        return new Empty<K, NatOrd<K>>(NatOrd.<K>getInstance());
    }

    public static <K, Ord extends Comparator<K>> Tree23Set<K, Ord> emptySet(Ord ord) {
        return new Empty(ord);
    }

    public abstract boolean contains(T elem);

    public abstract Tree23Set<T, Ord> add(T elem);

    public abstract Tree23Set<T, Ord> remove(T elem);

    abstract Tree23Set<T, Ord> removeMin();

    public abstract boolean isEmpty();

    protected abstract T getMin();

    protected abstract LazyList<T> getEntrySeq();

    static class Empty<T, Ord extends Comparator<T>> extends Tree23Set<T, Ord> {
        final Ord ord;

        public Empty(Ord ord) {
            this.ord = ord;
        }

        public Tree23Set<T, Ord> empty() {
            return this;
        }

        @Override
        public Tree23Set<T, Ord> add(T elem) {
            return new Node2<T, Ord>(this, elem);
        }

        @Override
        public boolean isEmpty() {
            return true;
        }

        @Override
        public boolean contains(T elem) {
            return false;
        }

        @Override
        Tree23Set<T, Ord> removeMin() {
            throw new UnsupportedOperationException();
        }

        @Override
        public Tree23Set<T, Ord> remove(T elem) {
            throw new UnsupportedOperationException();
        }

        @Override
        protected T getMin() {
            throw new UnsupportedOperationException();
        }

        @Override
        protected LazyList<T> getEntrySeq() {
            return LazyList.emptyStream();
        }

    }

    static class Node2<T, Ord extends Comparator<T>> extends Tree23Set<T, Ord> {
        final Empty<T, Ord> empty;
        final T elem;
        final Tree23Set<T, Ord> left;
        final Tree23Set<T, Ord> right;

        public Node2(Empty<T, Ord> empty, T elem) {
            this.empty = empty;
            this.elem = elem;
            this.left = empty;
            this.right = empty;
        }

        public Node2(Empty<T, Ord> empty, T elem, Tree23Set<T, Ord> left, Tree23Set<T, Ord> right) {
            this.empty = empty;
            this.elem = elem;
            this.left = left;
            this.right = right;
        }

        public Tree23Set<T, Ord> empty() {
            return empty;
        }

        @Override
        public Tree23Set<T, Ord> add(T elem) {
            final int cmp = empty.ord.compare(elem, this.elem);
            if (cmp < 0) {
                return insertLeft(elem);
            } else if (cmp > 0) {
                return insertRight(elem);
            } else {
                return new Node2<T, Ord>(empty, elem, left, right);
            }
        }

        private Tree23Set<T, Ord> balanceL(Tree23Set<T, Ord> newLeft, T value) {
            if (newLeft instanceof Empty || left instanceof Node2 && newLeft instanceof Node3) {
                if (right instanceof Node2) {
                    Node2<T, Ord> n = (Node2<T, Ord>) right;
                    return new Node3<T, Ord>(empty, newLeft, value, n.left, n.elem, n.right);
                } else {
                    Node3<T, Ord> n = (Node3<T, Ord>) right;
                    return new Node2<T, Ord>(empty, n.lElem, new Node2<T, Ord>(empty, value, newLeft, n.left), new Node2<T, Ord>(empty, n.rElem, n.mid, n.right));
                }
            }
            return new Node2<T, Ord>(empty, value, newLeft, right);
        }

        private Tree23Set<T, Ord> balanceR(Tree23Set<T, Ord> newRight, T value) {
            if (newRight instanceof Empty || right instanceof Node2 && newRight instanceof Node3) {
                if (left instanceof Node2) {
                    Node2<T, Ord> n = (Node2<T, Ord>) left;
                    return new Node3<T, Ord>(empty, n.left, n.elem, n.right, value, newRight);
                } else {
                    Node3<T, Ord> n = (Node3<T, Ord>) left;
                    return new Node2<T, Ord>(empty, n.rElem, new Node2<T, Ord>(empty, n.lElem, n.left, n.mid), new Node2<T, Ord>(empty, value, n.right, newRight));
                }
            }
            return new Node2<T, Ord>(empty, value, left, newRight);
        }

        private Tree23Set<T, Ord> insertRight(T elem) {
            if (right instanceof Empty) {
                return new Node3<T, Ord>(empty, this.elem, elem);
            }

            Tree23Set<T, Ord> newRight = right.add(elem);
            if (right instanceof Node3 && newRight instanceof Node2) {
                Node2<T, Ord> r = (Node2<T, Ord>) newRight;
                return new Node3<T, Ord>(empty, left, this.elem, r.left, r.elem, r.right);
            }

            return new Node2<T, Ord>(empty, this.elem, left, newRight);
        }

        private Tree23Set<T, Ord> insertLeft(T elem) {
            if (left instanceof Empty) {
                return new Node3<T, Ord>(empty, elem, this.elem);
            }

            Tree23Set<T, Ord> newLeft = left.add(elem);
            if (left instanceof Node3 && newLeft instanceof Node2) {
                Node2<T, Ord> l = (Node2<T, Ord>) newLeft;
                return new Node3<T, Ord>(empty, l.left, l.elem, l.right, this.elem, right);
            }

            return new Node2<T, Ord>(empty, this.elem, newLeft, right);
        }

        @Override
        public boolean isEmpty() {
            return false;
        }

        @Override
        public boolean contains(T elem) {
            final int cmp = empty.ord.compare(elem, this.elem);
            if (cmp == 0) {
                return true;
            } else if (cmp < 0) {
                return left.contains(elem);
            } else {
                return right.contains(elem);
            }
        }

        @Override
        public Tree23Set<T, Ord> remove(T elem) {
            final int cmp = empty.ord.compare(elem, this.elem);
            if (cmp == 0) {
                if (left instanceof Empty) {
                    return empty;
                } else {
                    T min = right.getMin();
                    Tree23Set<T, Ord> newRight = right.removeMin();
                    return balanceR(newRight, min);
                }
            } else if (cmp < 0) {
                return balanceL(left.remove(elem), this.elem);
            } else {
                return balanceR(right.remove(elem), this.elem);
            }
        }

        protected T getMin() {
            if (left instanceof Empty) {
                return elem;
            } else {
                return left.getMin();
            }
        }

        @Override
        Tree23Set<T, Ord> removeMin() {
            if (left instanceof Empty) {
                return empty;
            }

            Tree23Set<T, Ord> newLeft = left.removeMin();
            return balanceL(newLeft, this.elem);
        }

        @Override
        protected LazyList<T> getEntrySeq() {
            return left.getEntrySeq().append(right.getEntrySeq().cons(elem));
        }

    }

    static class Node3<T, Ord extends Comparator<T>> extends Tree23Set<T, Ord> {
        final Empty<T, Ord> empty;
        final Tree23Set<T, Ord> left;
        final T lElem;
        final Tree23Set<T, Ord> mid;
        final T rElem;
        final Tree23Set<T, Ord> right;

        public Node3(Empty<T, Ord> empty, T lElem, T rElem) {
            this.empty = empty;
            this.lElem = lElem;
            this.rElem = rElem;
            this.left = empty;
            this.mid = empty;
            this.right = empty;
        }

        public Node3(Empty<T, Ord> empty, Tree23Set<T, Ord> left, T lElem, Tree23Set<T, Ord> mid, T rElem, Tree23Set<T, Ord> right) {
            this.empty = empty;
            this.left = left;
            this.lElem = lElem;
            this.mid = mid;
            this.rElem = rElem;
            this.right = right;
        }

        public Tree23Set<T, Ord> empty() {
            return empty;
        }

        @Override
        public Tree23Set<T, Ord> add(T elem) {
            final int leftCmp = empty.ord.compare(elem, this.lElem);
            if (leftCmp < 0) {
                return insertLeft(elem);
            } else if (leftCmp == 0) {
                return new Node3<T, Ord>(empty, left, elem, mid, rElem, right);
            } else {
                final int rightCmp = empty.ord.compare(elem, this.rElem);
                if (rightCmp < 0) {
                    return insertMid(elem);
                } else if (rightCmp > 0) {
                    return insertRight(elem);
                } else {
                    return new Node3<T, Ord>(empty, left, lElem, mid, elem, right);
                }
            }
        }

        private Tree23Set<T, Ord> insertRight(T elem) {
            if (right instanceof Empty) {
                return new Node2<T, Ord>(empty, rElem, new Node2<T, Ord>(empty, lElem), new Node2<T, Ord>(empty, elem));
            }

            Tree23Set<T, Ord> newRight = right.add(elem);
            if (right instanceof Node3 && newRight instanceof Node2) {
                return new Node2<T, Ord>(empty, rElem, new Node2<T, Ord>(empty, lElem, left, mid), newRight);
            }

            return new Node3<T, Ord>(empty, left, lElem, mid, rElem, newRight);
        }

        private Tree23Set<T, Ord> insertMid(T elem) {
            Tree23Set<T, Ord> newMid = mid.add(elem);
            if (mid instanceof Node3 && newMid instanceof Node2 || mid instanceof Empty) {
                Node2<T, Ord> m = (Node2<T, Ord>) newMid;
                return new Node2<T, Ord>(empty, m.elem, new Node2<T, Ord>(empty, lElem, left, m.left), new Node2<T, Ord>(empty, rElem, m.right, right));
            }

            return new Node3<T, Ord>(empty, left, lElem, newMid, rElem, right);
        }

        private Tree23Set<T, Ord> insertLeft(T elem) {
            if (left instanceof Empty) {
                return new Node2<T, Ord>(empty, lElem, new Node2<T, Ord>(empty, elem), new Node2<T, Ord>(empty, rElem));
            }

            Tree23Set<T, Ord> newLeft = left.add(elem);
            if (left instanceof Node3 && newLeft instanceof Node2) {
                return new Node2<T, Ord>(empty, lElem, newLeft, new Node2<T, Ord>(empty, rElem, mid, right));
            }

            return new Node3<T, Ord>(empty, newLeft, lElem, mid, rElem, right);
        }

        @Override
        public boolean isEmpty() {
            return false;
        }

        @Override
        public boolean contains(T elem) {
            final int leftCmp = empty.ord.compare(elem, this.lElem);
            if (leftCmp < 0) {
                return left.contains(elem);
            } else if (leftCmp == 0) {
                return true;
            } else {
                final int rightCmp = empty.ord.compare(elem, this.rElem);
                if (rightCmp < 0) {
                    return mid.contains(elem);
                } else if (rightCmp > 0) {
                    return right.contains(elem);
                } else {
                    return true;
                }
            }
        }

        @Override
        Tree23Set<T, Ord> removeMin() {
            if (left instanceof Empty) {
                return new Node2<T, Ord>(empty, rElem);
            }

            Tree23Set<T, Ord> newLeft = left.removeMin();
            return balanceL(newLeft);
        }

        @Override
        public Tree23Set<T, Ord> remove(T elem) {
            final int leftCmp = empty.ord.compare(elem, this.lElem);
            if (leftCmp < 0) {
                return balanceL(left.remove(elem));
            } else if (leftCmp == 0) {
                if (right instanceof Empty) {
                    return new Node2<T, Ord>(empty, rElem);
                } else {
                    T min = mid.getMin();
                    Tree23Set<T, Ord> newMid = mid.removeMin();
                    return balanceM(newMid, min);
                }
            } else {
                final int rightCmp = empty.ord.compare(elem, this.rElem);
                if (rightCmp < 0) {
                    Tree23Set<T, Ord> newMid = mid.remove(elem);
                    return balanceM(newMid, lElem);
                } else if (rightCmp > 0) {
                    Tree23Set<T, Ord> newRight = right.remove(elem);
                    return balanceR(newRight, rElem);
                } else {
                    if (right instanceof Empty) {
                        return new Node2<T, Ord>(empty, lElem);
                    } else {
                        T min = right.getMin();
                        Tree23Set<T, Ord> newRight = right.removeMin();
                        return balanceR(newRight, min);
                    }
                }
            }
        }

        private Tree23Set<T, Ord> balanceL(Tree23Set<T, Ord> newLeft) {
            if (newLeft instanceof Empty || left instanceof Node2 && newLeft instanceof Node3) {
                if (mid instanceof Node2) {
                    Node2<T, Ord> n = (Node2<T, Ord>) mid;
                    return new Node2<T, Ord>(empty,
                            rElem,
                            new Node3<T, Ord>(empty, newLeft, lElem, n.left, n.elem, n.right),
                            right);
                } else {
                    Node3<T, Ord> n = (Node3<T, Ord>) mid;
                    return new Node3<T, Ord>(empty,
                            new Node2<T, Ord>(empty, lElem, newLeft, n.left),
                            n.lElem,
                            new Node2<T, Ord>(empty, n.rElem, n.mid, n.right),
                            rElem,
                            right);
                }
            } else {
                return new Node3<T, Ord>(empty, newLeft, lElem, mid, rElem, right);
            }
        }

        private Tree23Set<T, Ord> balanceR(Tree23Set<T, Ord> newRight, T min) {
            if (newRight instanceof Empty || right instanceof Node2 && newRight instanceof Node3) {
                if (mid instanceof Node2) {
                    Node2<T, Ord> n = (Node2<T, Ord>) mid;
                    return new Node2<T, Ord>(empty,
                            lElem,
                            left,
                            new Node3<T, Ord>(empty, n.left, n.elem, n.right, min, newRight));
                } else {
                    Node3<T, Ord> n = (Node3<T, Ord>) mid;
                    return new Node3<T, Ord>(empty,
                            left,
                            lElem,
                            new Node2<T, Ord>(empty, n.lElem, n.left, n.right),
                            n.rElem,
                            new Node2<T, Ord>(empty, min, n.right, newRight));
                }
            } else {
                return new Node3<T, Ord>(empty, left, lElem, mid, min, newRight);
            }
        }

        private Tree23Set<T, Ord> balanceM(Tree23Set<T, Ord> newMid, T min) {
            if (newMid instanceof Empty || mid instanceof Node2 && newMid instanceof Node3) {
                if (left instanceof Node2) {
                    Node2<T, Ord> n = (Node2<T, Ord>) left;
                    return new Node2<T, Ord>(empty, rElem,
                            new Node3<T, Ord>(empty, n.left, n.elem, n.right, min, newMid),
                            right);
                } else {
                    Node3<T, Ord> n = (Node3<T, Ord>) left;
                    return new Node3<T, Ord>(empty,
                            new Node2<T, Ord>(empty, n.lElem, n.left, n.mid),
                            n.rElem,
                            new Node2<T, Ord>(empty, min, n.right, newMid),
                            rElem,
                            right);
                }
            } else {
                return new Node3<T, Ord>(empty, left, min, newMid, rElem, right);
            }
        }

        @Override
        protected T getMin() {
            if (left instanceof Empty) {
                return lElem;
            } else {
                return left.getMin();
            }
        }

        @Override
        protected LazyList<T> getEntrySeq() {
            return left.getEntrySeq().append(new Memoize<LazyList<T>>() {

                @Override
                protected LazyList<T> call() {
                    return mid.getEntrySeq().cons(lElem).append(new Memoize<LazyList<T>>() {

                        @Override
                        protected LazyList<T> call() {
                            return right.getEntrySeq().cons(rElem);
                        }
                    });
                }
            });
        }

    }

    public Iterator<T> iterator() {
        return new SeqIterator<T>(getEntrySeq());
    }
}
