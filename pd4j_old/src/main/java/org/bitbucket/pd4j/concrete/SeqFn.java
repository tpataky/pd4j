/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.pd4j.concrete;

import org.bitbucket.pd4j.concept.Seq;
import org.bitbucket.pd4j.util.F2;

/**
 *
 * @author pataky
 */
class SeqFn {
    public static <U, T, SeqImpl extends Seq<T, SeqImpl>> U foldL(SeqImpl seq, U u, F2<U, U, T> fn) {
        if (seq.isEmpty()) {
            return u;
        }

        U folded = u;
        T cellValue = seq.head();
        SeqImpl remaining = seq.tail();
        while (true) {
            folded = fn.apply(folded, cellValue);
            if (remaining.isEmpty()) {
                break;
            }
            cellValue = remaining.head();
            remaining = remaining.tail();
        }
        return folded;
    }

    public static <T, S extends Seq<T, S>> S take(final S seq, final int i) {
        LinkedList<T> acc = LinkedList.emptyList();
        int taken = 0;
        S toTakeFrom = seq;
        while (taken < i) {
            T t = toTakeFrom.head();
            toTakeFrom = toTakeFrom.tail();
            acc = acc.cons(t);
            taken++;
        }

        S toReturn = seq.empty();
        while (!acc.isEmpty()) {
            toReturn.cons(acc.head());
            acc = acc.tail();
        }
        return toReturn;
    }

    public static <T, S extends Seq<T, S>> S drop(final S seq, final int i) {
        int dropped = 0;
        S toDropFrom = seq;
        while (dropped < i) {
            toDropFrom = toDropFrom.tail();
            dropped++;
        }
        return toDropFrom;
    }

    public static <T, SeqImpl extends Seq<T, SeqImpl>> boolean eq(SeqImpl a, SeqImpl b) {
        while (!(a.isEmpty() && b.isEmpty())) {
            if (a.isEmpty() ^ b.isEmpty()) {
                return false;
            }
            T thisValue = a.head();
            T thatValue = b.head();

            if (thisValue != thatValue && thisValue != null && !thisValue.equals(thatValue)) {
                return false;
            }

            a = a.tail();
            b = b.tail();
        }
        return true;
    }
}
