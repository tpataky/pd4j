/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.pd4j.concrete;

import java.util.Comparator;

/**
 *
 * @author pataky
 */
public class NatOrd<T extends Comparable<T>> implements Comparator<T> {

    private NatOrd() {
    }

    public int compare(T lhs, T rhs) {
        return lhs.compareTo(rhs);
    }

    public static <T extends Comparable<T>> NatOrd<T> getInstance() {
        return instance;
    }

    private static NatOrd instance = new NatOrd();
}
