/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.pd4j.concrete;

import java.util.Iterator;
import org.bitbucket.pd4j.concept.Vector;
import java.util.Arrays;

/**
 *
 * @author pataky
 */
public class Array<T> implements Vector<T, Array<T>>, Iterable<T> {

    private static final Array EMPTY_ARRAY = new Array();

    public static <T> Array<T> emptyArray() {
        return EMPTY_ARRAY;
    }

    private final Object[] elements;

    public static <T> Array<T> fromArray(T[] elements) {
        return new Array<T>(Arrays.copyOf(elements, elements.length));
    }

    public static <T> Array<T> fromVarArgs(T... elements) {
        return new Array<T>(Arrays.copyOf(elements, elements.length));
    }

    private Array() {
        this.elements = new Object[0];
    }

    private Array(Object[] elements) {
        this.elements = elements;
    }

    public Array<T> empty() {
        return new Array<T>();
    }

    public T get(int i) {
        return (T) elements[i];
    }

    public Array<T> resize(int newSize) {
        return new Array(Arrays.copyOf(elements, newSize));
    }

    public Array<T> set(int i, T t) {
        final Object[] newElements = Arrays.copyOf(elements, elements.length);
        newElements[i] = t;
        return new Array(newElements);
    }

    public int size() {
        return elements.length;
    }

    public boolean isEmpty() {
        return elements.length == 0;
    }

    public Iterator<T> iterator() {
        return new Iterator<T>() {

            private int idx = 0;

            public boolean hasNext() {
                return idx < elements.length;
            }

            public T next() {
                return (T) elements[idx++];
            }

            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
    }

}
