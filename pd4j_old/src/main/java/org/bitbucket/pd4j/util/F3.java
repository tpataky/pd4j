/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.pd4j.util;

/**
 *
 * @author pataky
 */
public interface F3<T, U1, U2, U3> {
    public T apply(U1 u1, U2 u2, U3 u3);
}
