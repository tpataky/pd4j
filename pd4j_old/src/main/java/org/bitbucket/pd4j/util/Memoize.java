package org.bitbucket.pd4j.util;

import java.util.function.Supplier;

public abstract class Memoize<T> implements Supplier {

    protected T value;
    protected boolean evaluated;

    protected Memoize() { }

    protected Memoize(T val) {
        value = val;
        evaluated = true;
    }

    public T get() {
        if (!evaluated) {
            value = call();
        }
        return value;
    }

    protected abstract T call();

    public static <T> Memoize<T> fn(Supplier f) {
        return new SuspFn<T>(f);
    }

    public static <T> Memoize<T> val(T v) {
        return new SuspVal<T>(v);
    }

    private static class SuspVal<T> extends Memoize<T> {

        public SuspVal(T val) {
            super(val);
        }

        @Override
        protected T call() {
            throw new IllegalStateException();
        }
    }

    private static class SuspFn<T> extends Memoize<T> {

        private final Supplier<T> fn;

        public SuspFn(Supplier<T> fn) {
            this.fn = fn;
        }

        @Override
        protected T call() {
            return fn.get();
        }
    }
}
