/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.pd4j.util;

/**
 *
 * @author pataky
 */
public interface F2<T, U1, U2> {
    public T apply(U1 u1, U2 u2);
}
