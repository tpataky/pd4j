/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.pd4j.concrete;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author pataky
 */
public class FingerTreeQueueTest {

    @Test
    public void shouldBecomeNonEmpty() {
        LazyFingerTreeQueue<Integer> q = LazyFingerTreeQueue.emptyQueue();

        assertTrue(q.isEmpty());
        assertFalse(q.cons(0).isEmpty());
        assertFalse(q.snoc(0).isEmpty());
    }

    @Test
    public void x() {
        LazyFingerTreeQueue<Integer> q = LazyFingerTreeQueue.emptyQueue();
        q = q.cons(1);
        q = q.cons(2);
        q = q.cons(3);
        q = q.cons(4);
        q = q.cons(5);
        q = q.cons(6);

        q = q.tail();
        q = q.tail();
        q = q.tail();
        q = q.tail();
        q = q.tail();
        q = q.tail();
    }

    @Test
    public void shouldComposeAndDecompose() {
        LazyFingerTreeQueue<Integer> q = LazyFingerTreeQueue.emptyQueue();

        LazyFingerTreeQueue<Integer> fwd = q;
        LazyFingerTreeQueue<Integer> rev = q;
        for (int i = 0; i < 30; i++) {
            fwd = fwd.snoc(i);
            rev = rev.cons(i);
        }

        for (int i = 0; i < 30; i++) {
            assertEquals(Integer.valueOf(i), fwd.head());
            assertEquals(Integer.valueOf(i), rev.last());
            fwd = fwd.tail();
            rev = rev.init();
        }
    }

    @Test
    public void shouldAppend() {
        LazyFingerTreeQueue<Integer> q = LazyFingerTreeQueue.emptyQueue();

        LazyFingerTreeQueue<Integer> fwd = q;
        LazyFingerTreeQueue<Integer> rev = q;

        for (int i = 0; i < 32; i++) {
            fwd = fwd.snoc(i);

            for (int j = 0; j < 32; j++) {
                rev = rev.cons(j);

                LazyFingerTreeQueue<Integer> append = fwd.append(rev);

                for (int k = 0; k <= i; k++) {
                    assertEquals(Integer.valueOf(k), append.head());
                    append = append.tail();
                }
                for (int k = 0; k < j; k++) {
                    assertEquals(Integer.valueOf(j - k), append.head());
                    append = append.tail();
                }
            }
        }

    }

}
