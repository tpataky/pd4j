/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.pd4j.concrete;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.bitbucket.pd4j.concrete.ArrayUtils.*;

/**
 *
 * @author pataky
 */
public class ArrayUtilsTest {
    @Test
    public void shouldRemoveFromMiddle() {
        assertEquals(new Integer[]{ 1, 3 },  remove(new Integer[] { 1, 2, 3 }, 1));
    }

    @Test
    public void shouldRemoveFromBeginning() {
        assertEquals(new Integer[]{ 2, 3 },  remove(new Integer[] { 1, 2, 3 }, 0));
    }

    @Test
    public void shouldRemoveFromEnd() {
        assertEquals(new Integer[]{ 1, 2 },  remove(new Integer[] { 1, 2, 3 }, 2));
    }

    @Test
    public void shouldAppend() {
        assertEquals(new Integer[]{ 1, 2, 3, 4 },  app(new Integer[] { 1, 2,}, new Integer[] { 3, 4,}));
    }
}
