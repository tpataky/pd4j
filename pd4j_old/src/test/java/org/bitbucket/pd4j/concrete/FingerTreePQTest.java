/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.pd4j.concrete;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author pataky
 */
public class FingerTreePQTest {
    @Test
    public void shouldReturnTheElementsInIncreasingOrder() {
        int NUM = 1000;

        FingerTreePQ<Integer, NatOrd<Integer>> q = FingerTreePQ.emptyQueue();
        for (int i = 0; i < NUM; i++) {
            q = q.cons(i).snoc((NUM * 2 - 1) - i);
        }

        for (int i = 0; i < (NUM * 2 - 1); i++) {
            assertEquals(Integer.valueOf(i), q.peek());
            q = q.pop();
        }
    }

    @Test
    public void shouldIterateOverTheElements() {
        int NUM = 1000;

        FingerTreePQ<Integer, NatOrd<Integer>> q = FingerTreePQ.emptyQueue();
        for (int i = 0; i < NUM; i++) {
            q = q.cons(i).snoc((NUM * 2 - 1) - i);
        }

        checkIncreasingOrderIteration(q, NUM * 2 - 1);
    }

    private void checkIncreasingOrderIteration(FingerTreePQ<Integer, NatOrd<Integer>> q, int max) {
        int expected = 0;
        for (int i : q) {
            assertEquals(expected, i);
            expected++;
        }
        assertEquals(max, expected - 1);
    }

    @Test
    public void shouldIterateOverTheElementsInIncreasingOrderBuiltFromAnyPermutation() {
        int[] numbers = new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };

        boolean hasNext = true;
        while (true) {
            FingerTreePQ<Integer, NatOrd<Integer>> q = FingerTreePQ.emptyQueue();
            for (int i : numbers) {
                q = q.cons(i);
            }

            checkIncreasingOrderIteration(q, 9);

            if (!hasNext) {
                break;
            }
            hasNext = PermutationUtils.nextPermutation(numbers);
        }
    }
}
