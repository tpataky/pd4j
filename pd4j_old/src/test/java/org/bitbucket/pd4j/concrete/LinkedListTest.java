/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.pd4j.concrete;

import org.bitbucket.pd4j.util.F1;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.bitbucket.pd4j.concrete.LinkedList.*;

/**
 *
 * @author pataky
 */
public class LinkedListTest {

    @Test
    public void shouldBeEqual() {
        assertEquals(create(1, 2, 3, 4, 5), emptyList().cons(5).cons(4).cons(3).cons(2).cons(1));
        assertEquals(emptyList(), emptyList().cons(5).cons(4).tail().tail());
    }

    @Test
    public void shouldNotBeEqual() {
        assertFalse(create("a", "b", 3, 4, 5).equals(emptyList().cons(5).cons(4).cons(3).cons(2).cons(1)));
        assertFalse(emptyList().equals(emptyList().cons(5).cons(4).tail()));
    }

    final F1<Integer, Integer> plus2 = new F1<Integer, Integer>() {
        public Integer apply(Integer u) {
            return u + 2;
        }
    };

    final F1<String, Object> show = new F1<String, Object>() {
        public String apply(Object u) {
            return u.toString();
        }
    };

    @Test
    public void shouldTransform() {
        final LinkedList<Integer> create = create(1, 2, 3, 4, 5);
        final LinkedList<Integer> transform = create.transform(plus2);

        assertEquals(create(3, 4, 5, 6, 7), transform);

        assertEquals(create("a", "b", "1", "2", "3"), LinkedList.<Object>create("a", "b", 1, 2, 3).transform(show));
    }

}
