/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.pd4j.concrete;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.bitbucket.pd4j.concrete.PermutationUtils.*;

/**
 *
 * @author pataky
 */
public class Tree23SetTest {

    @Test
    public void shouldBeEmpty() {
        Tree23Set<Integer, NatOrd<Integer>> t = Tree23Set.emptySet();
        assertTrue(t.isEmpty());
        assertTrue(isValid(t));
    }

    @Test
    public void shouldContainAllTheAddedElements() {
        Tree23Set<Integer, NatOrd<Integer>> t = Tree23Set.emptySet();
        for (int i = 0; i < 100; i++) {
            t = t.add(i);
            assertTrue(t.contains(i));
            assertTrue(isValid(t));
        }

        t = Tree23Set.emptySet();
        for (int i = 100; i > 0; i--) {
            t = t.add(i);
            assertTrue(t.contains(i));
            assertTrue(isValid(t));
        }
    }

    @Test
    public void shouldRemoveTheMinimum() {
        Tree23Set<Integer, NatOrd<Integer>> t = Tree23Set.emptySet();
        for (int i = 0; i < 100; i++) {
            t = t.add(i);
        }

        for (int i = 0; i < 100; i++) {
            t = t.removeMin();
            assertFalse(t.contains(i));
            assertTrue(isValid(t));
        }
    }

     @Test
    public void shouldIterateOverTheElementsInIncreasingOrder() {
        Tree23Set<Integer, NatOrd<Integer>> t = Tree23Set.emptySet();
        for (int i = 0; i < 10000; i++) {
            t = t.add(i);
        }

        Integer expected = 0;
        for (Integer i : t) {
            assertEquals(expected, i);
            expected++;
        }
    }

   @Test
    public void shouldRemainValid() {
        final int size = 8;
        int numbers[] = new int[size];
        for (int i = 0; i < size; i++) {
            numbers[i] = i;
        }

        while (nextPermutation(numbers)) {
            Tree23Set<Integer, NatOrd<Integer>> t = Tree23Set.emptySet();

            for (int i : numbers) {
                t = t.add(i);
                assertTrue(t.contains(i));
                assertTrue(PermutationUtils.toString(numbers) + ", " + i, isValid(t));
            }

            for (int i : numbers) {
                Tree23Set<Integer, NatOrd<Integer>> t_ = t.remove(i);

                assertTrue(PermutationUtils.toString(numbers) + ", " + i, isValid(t_));
            }
        }
    }

    public boolean isValid(Tree23Set t) {
        return isBalanced(t) && isOrdered(t);
    }
    public boolean isOrdered(Tree23Set t) {
        if (t instanceof Tree23Set.Empty) {
            return true;
        } else if (t instanceof Tree23Set.Node2) {
            Tree23Set.Node2 n = (Tree23Set.Node2) t;
            if (n.left instanceof Tree23Set.Empty) {
                return true;
            } else {
                Object max = max(n.left);
                Object min = min(n.right);
                return isOrdered(n.left) && isOrdered(n.right) && n.empty.ord.compare(max, n.elem) < 0 && n.empty.ord.compare(min, n.elem) > 0;
            }
        } else {
            Tree23Set.Node3 n = (Tree23Set.Node3) t;
            if (n.left instanceof Tree23Set.Empty) {
                return n.empty.ord.compare(n.lElem, n.rElem) < 0;
            } else {
                Object max = max(n.left);
                Object min = min(n.right);
                Object midMax = max(n.mid);
                Object midMin = min(n.mid);
                return isOrdered(n.left) && isOrdered(n.mid) && isOrdered(n.right) &&
                        n.empty.ord.compare(max, n.lElem) < 0 &&
                        n.empty.ord.compare(midMin, n.lElem) > 0 &&
                        n.empty.ord.compare(midMax, n.rElem) < 0 &&
                        n.empty.ord.compare(min, n.rElem) > 0;
            }
        }
    }
    public boolean isBalanced(Tree23Set t) {
        if (t instanceof Tree23Set.Empty) {
            return true;
        } else if (t instanceof Tree23Set.Node2) {
            Tree23Set.Node2 n = (Tree23Set.Node2) t;
            return isBalanced(n.left) && isBalanced(n.right) && depthOf(n.left) == depthOf(n.right);
        } else {
            Tree23Set.Node3 n = (Tree23Set.Node3) t;
            return isBalanced(n.left) && isBalanced(n.mid) && isBalanced(n.right) &&
                    depthOf(n.left) == depthOf(n.mid) && depthOf(n.mid) == depthOf(n.right);
        }
    }
    public int depthOf(Tree23Set t) {
        if (t instanceof Tree23Set.Empty) {
            return 0;
        } else if (t instanceof Tree23Set.Node2) {
            Tree23Set.Node2 n = (Tree23Set.Node2) t;
            return depthOf(n.left) + 1;
        } else {
            Tree23Set.Node3 n = (Tree23Set.Node3) t;
            return depthOf(n.left) + 1;
        }
    }

    Object max(Tree23Set t) {
        if (t instanceof Tree23Set.Node2) {
            Tree23Set.Node2 n = (Tree23Set.Node2) t;
            if (n.right instanceof Tree23Set.Empty) {
                return n.elem;
            } else {
                return max(n.right);
            }
        } else {
            Tree23Set.Node3 n = (Tree23Set.Node3) t;
            if (n.right instanceof Tree23Set.Empty) {
                return n.rElem;
            } else {
                return max(n.right);
            }
        }
    }

    Object min(Tree23Set t) {
        if (t instanceof Tree23Set.Node2) {
            Tree23Set.Node2 n = (Tree23Set.Node2) t;
            if (n.left instanceof Tree23Set.Empty) {
                return n.elem;
            } else {
                return max(n.left);
            }
        } else {
            Tree23Set.Node3 n = (Tree23Set.Node3) t;
            if (n.left instanceof Tree23Set.Empty) {
                return n.lElem;
            } else {
                return max(n.left);
            }
        }
    }
}
