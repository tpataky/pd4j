/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.pd4j.concrete;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author pataky
 */
public class TreeVectorTest {
    @Test
    public void shouldCreateAnEmptyVector() {
        TreeVector<Integer> v = TreeVector.emptyVector();

        assertTrue(v.isEmpty());
        assertEquals(0, v.size());
    }

    @Test
    public void shouldIncreaseTheSizeThenBecomeEmptyAgain() {
        TreeVector<Integer> vEmpty = TreeVector.emptyVector();

        TreeVector<Integer> vNonEmpty = vEmpty.resize(1);
        assertFalse(vNonEmpty.isEmpty());
        assertEquals(1, vNonEmpty.size());

        TreeVector<Integer> vEmptyAgain = vEmpty.resize(0);
        assertTrue(vEmptyAgain.isEmpty());
        assertEquals(0, vEmptyAgain.size());
    }

    @Test
    public void shouldCreateVectorsWithGivenSizes() {
        TreeVector<Integer> v1 = TreeVector.create(3);
        TreeVector<Integer> v2 = TreeVector.create(32);
        TreeVector<Integer> v3 = TreeVector.create(66);
        TreeVector<Integer> v4 = TreeVector.create(2048);

        assertEquals(3, v1.size());
        assertEquals(32, v2.size());
        assertEquals(66, v3.size());
        assertEquals(2048, v4.size());
    }

    @Test
    public void shouldStoreTheValues() {
        TreeVector<Integer> v = TreeVector.create(3);
        v = v.set(0, 1).set(1, 2).set(2, 3);

        assertEquals(Integer.valueOf(1), v.get(0));
        assertEquals(Integer.valueOf(2), v.get(1));
        assertEquals(Integer.valueOf(3), v.get(2));
    }

    @Test
    public void shouldHaveThePrefixAfterShrink() {
        TreeVector<Integer> v = TreeVector.emptyVector();

        v = v.resize(32769);
        for (int i = 0; i < 32769; i++) {
            v = v.set(i, i);
        }

        v = v.resize(16384);

        v.get(4096);
        for (int i = 0; i < 16384; i++) {
            assertEquals(Integer.valueOf(i), v.get(i));
        }

        assertEquals(16384, v.size());
    }

    @Test
    public void shouldHaveThePrefixAfterShrink2() {
        TreeVector<Integer> v = TreeVector.emptyVector();

        v = v.resize(32769);
        for (int i = 0; i < 32769; i++) {
            v = v.set(i, i);
        }

        v = v.resize(3);

        for (int i = 0; i < 3; i++) {
            assertEquals(Integer.valueOf(i), v.get(i));
        }

        assertEquals(3, v.size());
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void shouldThrow1() {
        TreeVector.emptyVector().get(0);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void shouldThrow2() {
        TreeVector.emptyVector().set(0, 1);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void shouldThrow3() {
        TreeVector.create(2).get(-1);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void shouldThrow4() {
        TreeVector.create(2).get(2);
    }

    @Test
    public void shouldIterateOverTheEntries() {
        TreeVector<Integer> v = TreeVector.emptyVector();
        final int size = 32769;

        v = v.resize(size);
        for (int i = 0; i < size; i++) {
            v = v.set(i, i);
        }

        Integer i = 0;
        for (Integer entry : v) {
            assertEquals(i, entry);
            i++;
        }
    }
}
