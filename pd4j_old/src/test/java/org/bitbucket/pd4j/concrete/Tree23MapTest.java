/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.pd4j.concrete;

import org.bitbucket.pd4j.util.Pair;
import org.bitbucket.pd4j.util.Option;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.bitbucket.pd4j.concrete.PermutationUtils.*;

/**
 *
 * @author pataky
 */
public class Tree23MapTest {

    @Test
    public void shouldBeEmpty() {
        Tree23Map<Integer, Integer, NatOrd<Integer>> t = Tree23Map.emptyMap();
        assertTrue(t.isEmpty());
        assertTrue(isValid(t));
    }

    @Test
    public void shouldContainAllTheAddedElements() {
        Tree23Map<Integer, Integer, NatOrd<Integer>> t = Tree23Map.emptyMap();
        for (int i = 0; i < 100; i++) {
            t = t.put(i, i);
            assertEquals(Option.some(i), t.get(i));
            assertTrue(isValid(t));
        }

        t = Tree23Map.emptyMap();
        for (int i = 100; i > 0; i--) {
            t = t.put(i, i);
            assertEquals(Option.some(i), t.get(i));
            assertTrue(isValid(t));
        }
    }

    @Test
    public void shouldRemoveTheMinimum() {
        Tree23Map<Integer, Integer, NatOrd<Integer>> t = Tree23Map.emptyMap();
        for (int i = 0; i < 100; i++) {
            t = t.put(i, i);
        }

        for (int i = 0; i < 100; i++) {
            t = t.removeMin();
            assertEquals(Option.nothing(), t.get(i));
            assertTrue(isValid(t));
        }
    }

    @Test
    public void shouldIterateOverTheElementsInIncreasingOrder() {
        Tree23Map<Integer, Integer, NatOrd<Integer>> t = Tree23Map.emptyMap();
        for (int i = 0; i < 10000; i++) {
            t = t.put(i, i);
        }

        Integer expected = 0;
        for (Pair<Integer, Integer> p : t) {
            assertEquals(expected, p.first);
            assertEquals(expected, p.second);
            expected++;
        }
    }

    @Test
    public void shouldRemainValid() {
        final int size = 8;
        int numbers[] = new int[size];
        for (int i = 0; i < size; i++) {
            numbers[i] = i;
        }

        while (nextPermutation(numbers)) {
            Tree23Map<Integer, Integer, NatOrd<Integer>> t = Tree23Map.emptyMap();

            for (int i : numbers) {
                t = t.put(i, i);
                assertEquals(Option.some(i), t.get(i));
                assertTrue(PermutationUtils.toString(numbers) + ", " + i, isValid(t));
            }

            for (int i : numbers) {
                Tree23Map<Integer, Integer, NatOrd<Integer>> t_ = t.remove(i);

                assertTrue(PermutationUtils.toString(numbers) + ", " + i, isValid(t_));
            }
        }
    }

    public boolean isValid(Tree23Map t) {
        return isBalanced(t) && isOrdered(t);
    }
    public boolean isOrdered(Tree23Map t) {
        if (t instanceof Tree23Map.Empty) {
            return true;
        } else if (t instanceof Tree23Map.Node2) {
            Tree23Map.Node2 n = (Tree23Map.Node2) t;
            if (n.left instanceof Tree23Map.Empty) {
                return true;
            } else {
                Object max = max(n.left);
                Object min = min(n.right);
                return isOrdered(n.left) && isOrdered(n.right) && n.empty.ord.compare(max, n.key) < 0 && n.empty.ord.compare(min, n.key) > 0;
            }
        } else {
            Tree23Map.Node3 n = (Tree23Map.Node3) t;
            if (n.left instanceof Tree23Map.Empty) {
                return n.empty.ord.compare(n.lKey, n.rKey) < 0;
            } else {
                Object max = max(n.left);
                Object min = min(n.right);
                Object midMax = max(n.mid);
                Object midMin = min(n.mid);
                return isOrdered(n.left) && isOrdered(n.mid) && isOrdered(n.right) &&
                        n.empty.ord.compare(max, n.lKey) < 0 &&
                        n.empty.ord.compare(midMin, n.lKey) > 0 &&
                        n.empty.ord.compare(midMax, n.rKey) < 0 &&
                        n.empty.ord.compare(min, n.rKey) > 0;
            }
        }
    }
    public boolean isBalanced(Tree23Map t) {
        if (t instanceof Tree23Map.Empty) {
            return true;
        } else if (t instanceof Tree23Map.Node2) {
            Tree23Map.Node2 n = (Tree23Map.Node2) t;
            return isBalanced(n.left) && isBalanced(n.right) && depthOf(n.left) == depthOf(n.right);
        } else {
            Tree23Map.Node3 n = (Tree23Map.Node3) t;
            return isBalanced(n.left) && isBalanced(n.mid) && isBalanced(n.right) &&
                    depthOf(n.left) == depthOf(n.mid) && depthOf(n.mid) == depthOf(n.right);
        }
    }
    public int depthOf(Tree23Map t) {
        if (t instanceof Tree23Map.Empty) {
            return 0;
        } else if (t instanceof Tree23Map.Node2) {
            Tree23Map.Node2 n = (Tree23Map.Node2) t;
            return depthOf(n.left) + 1;
        } else {
            Tree23Map.Node3 n = (Tree23Map.Node3) t;
            return depthOf(n.left) + 1;
        }
    }

    Object max(Tree23Map t) {
        if (t instanceof Tree23Map.Node2) {
            Tree23Map.Node2 n = (Tree23Map.Node2) t;
            if (n.right instanceof Tree23Map.Empty) {
                return n.key;
            } else {
                return max(n.right);
            }
        } else {
            Tree23Map.Node3 n = (Tree23Map.Node3) t;
            if (n.right instanceof Tree23Map.Empty) {
                return n.rKey;
            } else {
                return max(n.right);
            }
        }
    }

    Object min(Tree23Map t) {
        if (t instanceof Tree23Map.Node2) {
            Tree23Map.Node2 n = (Tree23Map.Node2) t;
            if (n.left instanceof Tree23Map.Empty) {
                return n.key;
            } else {
                return max(n.left);
            }
        } else {
            Tree23Map.Node3 n = (Tree23Map.Node3) t;
            if (n.left instanceof Tree23Map.Empty) {
                return n.lKey;
            } else {
                return max(n.left);
            }
        }
    }
}
