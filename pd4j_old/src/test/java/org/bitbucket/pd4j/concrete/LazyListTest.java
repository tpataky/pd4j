/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.pd4j.concrete;

import org.bitbucket.pd4j.util.F1;
import org.bitbucket.pd4j.util.Option;
import org.bitbucket.pd4j.util.Pair;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author pataky
 */
public class LazyListTest {

    @Test
    public void castabilityCheck() {
        Object[] arrayOfStrings = new Object[] { "A", "B", "C" };

        LazyList<Object> stringStream = LazyList.fromArray(arrayOfStrings);

        assertEquals("A", stringStream.head());
        assertEquals("B", stringStream.tail().head());
        assertEquals("C", stringStream.tail().tail().head());
        assertSame(LazyList.emptyStream(), stringStream.tail().tail().tail());
    }

    @Test
    public void shouldTakeTheGivenNumberOfElemnts() {
        LazyList<Integer> s = LazyList.fromVarArgs(1, 2, 3, 4, 5);

        assertTrue(SeqFn.eq(LazyList.fromVarArgs(1, 2, 3), s.take(3)));
        assertTrue(SeqFn.eq(LazyList.fromVarArgs(1, 2, 3, 4, 5), s.take(5)));
        assertTrue(SeqFn.eq(LazyList.<Integer>emptyStream(), s.take(0)));
    }

    LazyList<Integer> decreasingFrom5 = LazyList.unfold(5, new F1<Option<Pair<Integer, Integer>>, Integer>() {
        public Option<Pair<Integer, Integer>> apply(Integer u) {
            if (u >= 0) {
                return Option.some(Pair.create(u, u - 1));
            } else {
                return Option.nothing();
            }
        }
    });

    @Test
    public void shouldTransform() {
        assertEquals(LazyList.fromVarArgs(10, 8, 6, 4, 2, 0), decreasingFrom5.transform(new F1<Integer, Integer>() {
            public Integer apply(Integer u) {
                return 2 * u;
            }
        }));
    }

    @Test
    public void shouldTransformLazily() {
        decreasingFrom5.transform(new F1<Integer, Integer>() {
            public Integer apply(Integer u) {
                return 2 / u;
            }
        });
    }
}
