/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.pd4j.concrete;

/**
 *
 * @author pataky
 */
public class PermutationUtils {

    public static boolean nextPermutation(int[] numbers) {
        int i = numbers.length - 1;
        if (i == 0) {
            return false;
        }
        while (true) {
            int j = i;
            i--;
            if (numbers[i] < numbers[j]) {
                int k = numbers.length;
                while (!(numbers[i] < numbers[--k])) {}
                swap(numbers, i, k);
                reverse(numbers, j, numbers.length - 1);
                return true;
            }
            if (i == 0) {
                reverse(numbers, 0, numbers.length - 1);
                return false;
            }
        }
    }

    private static void swap(int[] numbers, int i, int j) {
        int t = numbers[j];
        numbers[j] = numbers[i];
        numbers[i] = t;
    }

    private static void reverse(int[] numbers, int beg, int end) {
        while (beg < end) {
            swap(numbers, beg, end);
            beg++;
            end--;
        }
    }

    public static String toString(int[] numbers) {
        StringBuilder b = new StringBuilder();
        b.append('[');
        for (int i = 0; i < numbers.length; i++) {
            b.append(numbers[i]);
            b.append(' ');
        }
        b.append(']');
        return b.toString();
    }

}
