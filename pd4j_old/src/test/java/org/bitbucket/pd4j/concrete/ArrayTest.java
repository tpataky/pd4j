/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.pd4j.concrete;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author pataky
 */
public class ArrayTest {

    @Test
    public void shouldBeEmpty() {
        Array<Object> array = Array.emptyArray();
        assertTrue(array.isEmpty());
        assertEquals(0, array.size());
    }

    @Test
    public void shouldCreateAnArrayAsCopyOfInput() {
        Integer[] values = new Integer[] { 1, 2, 3, 4 };
        Array<Integer> array = Array.fromArray(values);

        assertEquals(values[0], array.get(0));
        assertEquals(values[1], array.get(1));
        assertEquals(values[2], array.get(2));
        assertEquals(values[3], array.get(3));
    }

    @Test
    public void shouldUpdatingCreateAnUpdatedCopy() {
        Array<Integer> array = Array.fromVarArgs(1, 2, 3, 4);
        Array<Integer> updated = array.set(1, 5);

        assertEquals(Integer.valueOf(5), updated.get(1));
        assertEquals(Integer.valueOf(2), array.get(1));
    }

    @Test
    public void shouldIterateOverTheEntries() {
        Integer expected = 1;
        for (Integer i : Array.fromVarArgs(1, 2, 3, 4, 5)) {
            assertEquals(expected, i);
            expected++;
        }
    }
}
