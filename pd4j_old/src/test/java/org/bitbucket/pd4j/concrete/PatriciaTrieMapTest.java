/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.pd4j.concrete;

import java.util.NoSuchElementException;
import org.bitbucket.pd4j.util.Pair;
import org.bitbucket.pd4j.concept.Seq;
import org.bitbucket.pd4j.util.Option;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author pataky
 */
public class PatriciaTrieMapTest {

    private static final int[] pow2 = {
        -2147483648,
        1073741824,
        536870912,
        268435456,
        134217728,
        67108864,
        33554432,
        16777216,
        8388608,
        4194304,
        2097152,
        1048576,
        524288,
        262144,
        131072,
        65536,
        32768,
        16384,
        8192,
        4096,
        2048,
        1024,
        512,
        256,
        128,
        64,
        32,
        16,
        8,
        4,
        2,
        1,
    };

    @Test
    public void shouldReturnTheInsertedValues() {

        PatriciaTrieMap<Integer, Integer> t = PatriciaTrieMap.<Integer, Integer>emptyMap();

        for (int p : pow2) {
            t = t.put(p, p);
            assertEquals(Option.some(p), t.get(p));
        }
        for (int p : pow2) {
            t = t.put(p - 1, p);
            assertEquals(Option.some(p), t.get(p - 1));
        }

    }

    @Test
    public void shouldGiveNothing() {
        PatriciaTrieMap<Integer, Integer> m = PatriciaTrieMap.<Integer, Integer>emptyMap();
        assertTrue(m.get(0).isNothing());
    }

    @Test
    public void shouldGiveBackTheInsertedValue() {
        PatriciaTrieMap<Integer, Integer> m = PatriciaTrieMap.<Integer, Integer>emptyMap();

        m.put(0, Integer.SIZE).get(0).fold(
                () -> fail(),
                t -> assertEquals(Integer.SIZE, t.intValue())
                );
    }

    static class TestKey {
        final int hash;
        final int value;

        public TestKey(int hash, int value) {
            this.hash = hash;
            this.value = value;
        }

        @Override
        public boolean equals(Object obj) {
            return value == ((TestKey) obj).value;
        }

        @Override
        public int hashCode() {
            return hash;
        }
    }

    @Test
    public void shouldReturnTheCorrectValueWithCollidingKeys() {
        final TestKey k1 = new TestKey(0, 1);
        final TestKey k2 = new TestKey(0, 2);

        PatriciaTrieMap<TestKey, Integer> m = PatriciaTrieMap.<TestKey, Integer>emptyMap();

        m = m.put(k1, k1.value).put(k2, k2.value);

        m.get(k1).fold(
                () -> fail(),
                t -> assertEquals(k1.value, t.intValue())
        );

        m.get(k2).fold(
                () -> fail(),
                t -> assertEquals(k2.value, t.intValue())
        );
    }

    @Test
    public void shouldRemoveTheCorrectValueWithCollidingKeys() {
        final TestKey k1 = new TestKey(0, 1);
        final TestKey k2 = new TestKey(0, 2);

        PatriciaTrieMap<TestKey, Integer> m = PatriciaTrieMap.<TestKey, Integer>emptyMap();

        m = m.put(k1, k1.value).put(k2, k2.value);

        assertTrue(m.remove(k1).get(k1).isNothing());

        assertTrue(m.remove(k2).get(k2).isNothing());
    }

    @Test
    public void shouldRemoveTheEntry() {
        PatriciaTrieMap<Integer, Integer> m = PatriciaTrieMap.<Integer, Integer>emptyMap();
        m = m.put(1, 1).remove(1);

        assertTrue(m.get(1).isNothing());
    }

    @Test
    public void shouldRemoveTheEntries1() {
        PatriciaTrieMap<Integer, Integer> m = PatriciaTrieMap.<Integer, Integer>emptyMap();
        m = m.put(1, 1).put(0, 0);

        assertTrue(m.remove(1).get(1).isNothing());
        assertEquals(Option.some(0), m.remove(1).get(0));

        assertTrue(m.remove(0).get(0).isNothing());
        assertEquals(Option.some(1), m.remove(0).get(1));
    }

    @Test
    public void shouldRemoveTheEntries2() {
        PatriciaTrieMap<Integer, Integer> m = PatriciaTrieMap.<Integer, Integer>emptyMap();
        for (int i = 0; i < 32; i++) {
            m = m.put(i, i);
        }

        for (int i = 0; i < 32; i++) {
            PatriciaTrieMap<Integer, Integer> r = m.remove(i);
            assertTrue(r.get(i).isNothing());

            for (int j = 0; j < 32; j++) {
                if (j != i) {
                    assertEquals(Option.some(j), r.get(j));
                }
            }
        }

    }

    @Test
    public void shouldRemoveTheElementFromTheArray() {
        Object[] input = new Integer[] { 1, 2, 3 };
        Object[] output = PatriciaTrieMap.Bucket.removeElement(input, 0);

        assertEquals(input[1], output[0]);
        assertEquals(input[2], output[1]);

        output = PatriciaTrieMap.Bucket.removeElement(input, 1);

        assertEquals(input[0], output[0]);
        assertEquals(input[2], output[1]);

        output = PatriciaTrieMap.Bucket.removeElement(input, 2);

        assertEquals(input[0], output[0]);
        assertEquals(input[1], output[1]);
    }

    @Test
    public void shouldUpdateTheValue() {
        PatriciaTrieMap<Integer, Integer> m = PatriciaTrieMap.emptyMap();

        m.put(1, 1).put(1, 2).get(1).fold(
                () -> fail(),
                t -> assertEquals(2, t.intValue())
        );
    }

    @Test
    public void shouldReturnTheSequenceOfEntries() {
        PatriciaTrieMap<Integer, Integer> m = PatriciaTrieMap.emptyMap();

        Seq<Pair<Integer, Integer>, ?> entrySeq = m.put(1, 1).put(1, 2).put(2, 0).put(3, 4).put(4, 1).entrySeq();

        assertTrue(
                removeAll(
                    entrySeq,
                    LinkedList.create(Pair.create(1, 2), Pair.create(2, 0), Pair.create(3, 4), Pair.create(4, 1))
                ).isEmpty());
    }

    private <T> Seq<T, ?> removeAll(Seq<T, ?> a, Seq<T, ?> b) {
        Seq<T, ?> removeFrom = a;
        Seq<T, ?> toRemove = b;
        while (!toRemove.isEmpty()) {
            T head = toRemove.head();
            toRemove = toRemove.tail();
            removeFrom = remove(removeFrom, head);
        }
        return removeFrom;
    }

    private <T> Seq<T, ?> remove(Seq<T, ?> a, T elem) {
        Seq<T, ?> remainder = a;
        Seq<T, ?> unfolded = a.empty();
        while (!remainder.isEmpty()) {
            T head = remainder.head();
            remainder = remainder.tail();
            if (head.equals(elem)) {
                while (!unfolded.isEmpty()) {
                    remainder = remainder.cons(unfolded.head());
                    unfolded = unfolded.tail();
                }
                return remainder;
            }
            unfolded = unfolded.cons(head);
        }
        throw new NoSuchElementException();
    }

    @Test
    public void shouldIterateOverTheEntries() {
        PatriciaTrieMap<Integer, Integer> m = PatriciaTrieMap.emptyMap();
        m = m.put(1, 1).put(2, 2).put(3, 3);
        Seq<Pair<Integer, Integer>, ?> entrySeq = m.entrySeq();
        for (Pair<Integer, Integer> p : m) {
            assertEquals(entrySeq.head(), p);
            entrySeq = entrySeq.tail();
        }
    }
}
