/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.pd4j.concrete;

import org.bitbucket.pd4j.concrete.LinkedList;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.bitbucket.pd4j.concrete.SeqFn.*;

/**
 *
 * @author pataky
 */
public class SeqFnTest {
    @Test
    public void x() {
        LinkedList<Integer> s = LinkedList.create(1, 2, 3, 4, 5);
        assertTrue(eq(s, drop(s, 0)));
    }
}
