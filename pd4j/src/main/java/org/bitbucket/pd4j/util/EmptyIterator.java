package org.bitbucket.pd4j.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Spliterator;
import java.util.function.Consumer;

public final class EmptyIterator<T> implements Iterator<T>, Spliterator<T> {
    private final static EmptyIterator<?> INSTANCE = new EmptyIterator<>();

    @SuppressWarnings("unchecked")
    public static <T> EmptyIterator<T> instance() {
        return (EmptyIterator<T>) INSTANCE;
    }

    @Override
    public boolean hasNext() {
        return false;
    }

    @Override
    public T next() {
        throw new NoSuchElementException();
    }

    @Override
    public boolean tryAdvance(Consumer<? super T> action) {
        return false;
    }

    @Override
    public Spliterator<T> trySplit() {
        return null;
    }

    @Override
    public long estimateSize() {
        return 0;
    }

    @Override
    public int characteristics() {
        return Spliterator.CONCURRENT | Spliterator.DISTINCT | Spliterator.IMMUTABLE | Spliterator.NONNULL
                | Spliterator.ORDERED | Spliterator.SIZED | Spliterator.SORTED | Spliterator.SUBSIZED;
    }

    @Override
    public void forEachRemaining(Consumer<? super T> action) {
        // do nothing
    }

    @Override
    public Comparator<? super T> getComparator() {
        return null;
    }
}
