package org.bitbucket.pd4j;

import org.bitbucket.pd4j.util.EmptyIterator;
import org.bitbucket.pd4j.util.Function3;
import org.bitbucket.pd4j.util.Function5;

import java.util.ArrayDeque;
import java.util.Comparator;
import java.util.Deque;
import java.util.Iterator;
import java.util.Objects;
import java.util.Spliterator;
import java.util.function.Consumer;
import java.util.function.Supplier;

public abstract class PersistentTreeSet<E> implements
        PersistentSortedSet<E, PersistentTreeSet<E>>,
        Iterable<E>
{

    public static <E extends Comparable<E>> PersistentTreeSet<E> empty() {
        return new Empty<E>(Comparator.naturalOrder());
    }

    public static <E> PersistentTreeSet<E> empty(Comparator<E> cmp) {
        return new Empty<>(cmp);
    }

    @Override
    public int size() {
        return fold(
                () -> 0,
                (leftSize, elem, rightSize) -> leftSize + rightSize + 1,
                (leftSize, lElem, midSize, rElem, rightSize) -> leftSize + midSize + rightSize + 2
        );
    }

    public abstract <R> R fold(Supplier<R> ifEmpty, Function3<R, E, R, R> ifNode2, Function5<R, E, R, E, R, R> ifNode3);

    protected abstract RemoveMin<E> removeMin();

    protected abstract PersistentTreeSet<E> rightChild();

    protected abstract PersistentTreeSet<E> leftChild();

    @Override
    public int hashCode() {
        return fold(
                () -> 31,
                (leftHash, elem, rightHash) -> leftHash + elem.hashCode() * 37 + rightHash,
                (leftHash, lElem, midHash, rElem, rightHash) -> leftHash + lElem.hashCode() * 37 + midHash + rElem.hashCode() * 37 + rightHash
        );
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof PersistentTreeSet)) {
            return false;
        }

        @SuppressWarnings("unchecked")
        PersistentTreeSet<E> that = (PersistentTreeSet<E>) obj;

        Iterator<E> thisIterator = this.iterator();
        Iterator<E> thatIterator = that.iterator();

        boolean equal = thisIterator.hasNext() == thatIterator.hasNext();
        while (equal && thisIterator.hasNext()) {
            E thisElem = thisIterator.next();
            E thatElem = thatIterator.next();
            equal = Objects.equals(thisElem, thatElem) && (thisIterator.hasNext() == thatIterator.hasNext());
        }
        return equal;
    }

    @Override
    public String toString() {
        StringBuilder b = fold(
                () -> new StringBuilder("[]"),

                (leftString, elem, rightChild) ->
                        leftString.insert(0, "[").append(", ")
                                .append(elem).append(", ")
                                .append(rightChild)
                                .append("]"),

                (leftString, lElem, midString, rElem, rightString) ->
                        leftString.insert(0, "[").append(", ")
                                .append(lElem).append(", ")
                                .append(midString).append(", ")
                                .append(rElem).append(", ")
                                .append(rightString)
                                .append("]")
        );

        return b.toString();
    }

    @Override
    public Iterator<E> iterator() {
        return new TreeIterator<>(this);
    }

    @Override
    public Spliterator<E> spliterator() {
        return new TreeSpliterator<E>(this);
    }

    static final class Empty<E> extends PersistentTreeSet<E> {
        final Comparator<E> cmp;

        public Empty(Comparator<E> cmp) {
            this.cmp = cmp;
        }

        @Override
        public PersistentTreeSet<E> add(E elem) {
            return new Node2<>(this, elem);
        }

        @Override
        public boolean isEmpty() {
            return true;
        }

        @Override
        public boolean contains(E elem) {
            return false;
        }

        @Override
        protected RemoveMin<E> removeMin() {
            throw new UnsupportedOperationException();
        }

        @Override
        public PersistentTreeSet<E> remove(E elem) {
            return this;
        }

        @Override
        public <R> R fold(Supplier<R> ifEmpty, Function3<R, E, R, R> ifNode2, Function5<R, E, R, E, R, R> ifNode3) {
            return ifEmpty.get();
        }

        @Override
        protected PersistentTreeSet<E> rightChild() {
            throw new UnsupportedOperationException();
        }

        @Override
        protected PersistentTreeSet<E> leftChild() {
            throw new UnsupportedOperationException();
        }

        @Override
        public Iterator<E> iterator() {
            return EmptyIterator.instance();
        }
    }

    static class Node2<E> extends PersistentTreeSet<E> {
        final Empty<E> empty;
        final E elem;
        final PersistentTreeSet<E> left;
        final PersistentTreeSet<E> right;

        public Node2(Empty<E> empty, E elem) {
            this.empty = empty;
            this.elem = elem;
            this.left = empty;
            this.right = empty;
        }

        public Node2(Empty<E> empty, E elem, PersistentTreeSet<E> left, PersistentTreeSet<E> right) {
            this.empty = empty;
            this.elem = elem;
            this.left = left;
            this.right = right;
        }

        @Override
        public PersistentTreeSet<E> add(E elem) {
            final int cmp = empty.cmp.compare(elem, this.elem);
            if (cmp < 0) {
                return insertLeft(elem);
            } else if (cmp > 0) {
                return insertRight(elem);
            } else {
                return this;
            }
        }

        private PersistentTreeSet<E> insertRight(E elem) {
            if (right instanceof Empty) {
                return new Node3<>(empty, this.elem, elem);
            }

            PersistentTreeSet<E> newRight = right.add(elem);
            if (right == newRight) {
                return this;
            }

            if (right instanceof Node3 && newRight instanceof Node2) {
                Node2<E> r = (Node2<E>) newRight;
                return new Node3<>(empty, left, this.elem, r.left, r.elem, r.right);
            }

            return new Node2<>(empty, this.elem, left, newRight);
        }

        private PersistentTreeSet<E> insertLeft(E elem) {
            if (left instanceof Empty) {
                return new Node3<>(empty, elem, this.elem);
            }

            PersistentTreeSet<E> newLeft = left.add(elem);
            if (left == newLeft) {
                return this;
            }

            if (left instanceof Node3 && newLeft instanceof Node2) {
                Node2<E> l = (Node2<E>) newLeft;
                return new Node3<>(empty, l.left, l.elem, l.right, this.elem, right);
            }

            return new Node2<>(empty, this.elem, newLeft, right);
        }

        @Override
        public boolean isEmpty() {
            return false;
        }

        @Override
        public boolean contains(E elem) {
            final int cmp = empty.cmp.compare(elem, this.elem);
            if (cmp == 0) {
                return true;
            } else if (cmp < 0) {
                return left.contains(elem);
            } else {
                return right.contains(elem);
            }
        }

        @Override
        public PersistentTreeSet<E> remove(E elem) {
            final int cmp = empty.cmp.compare(elem, this.elem);
            if (cmp == 0) {
                if (left instanceof Empty) {
                    return empty;
                } else {
                    RemoveMin<E> removeMin = right.removeMin();
                    return balanceR(removeMin.result, removeMin.min);
                }
            } else if (cmp < 0) {
                return balanceL(left.remove(elem), this.elem);
            } else {
                return balanceR(right.remove(elem), this.elem);
            }
        }

        private PersistentTreeSet<E> balanceL(PersistentTreeSet<E> newLeft, E value) {
            if (newLeft == left) {
                return this;
            }
            if (newLeft instanceof Empty || left instanceof Node2 && newLeft instanceof Node3) {
                if (right instanceof Node2) {
                    Node2<E> n = (Node2<E>) right;
                    return new Node3<>(empty, newLeft, value, n.left, n.elem, n.right);
                } else {
                    Node3<E> n = (Node3<E>) right;
                    return new Node2<>(empty, n.lElem, new Node2<>(empty, value, newLeft, n.left), new Node2<>(empty, n.rElem, n.mid, n.right));
                }
            }
            return new Node2<>(empty, value, newLeft, right);
        }

        private PersistentTreeSet<E> balanceR(PersistentTreeSet<E> newRight, E value) {
            if (newRight == right) {
                return this;
            }
            if (newRight instanceof Empty || right instanceof Node2 && newRight instanceof Node3) {
                if (left instanceof Node2) {
                    Node2<E> n = (Node2<E>) left;
                    return new Node3<>(empty, n.left, n.elem, n.right, value, newRight);
                } else {
                    Node3<E> n = (Node3<E>) left;
                    return new Node2<>(empty, n.rElem, new Node2<>(empty, n.lElem, n.left, n.mid), new Node2<>(empty, value, n.right, newRight));
                }
            }
            return new Node2<>(empty, value, left, newRight);
        }

        @Override
        protected RemoveMin<E> removeMin() {
            if (left instanceof Empty) {
                return new RemoveMin<>(elem, empty);
            }

            RemoveMin<E> removeMin = left.removeMin();
            removeMin.result = balanceL(removeMin.result, this.elem);
            return removeMin;
        }

        @Override
        public <R> R fold(Supplier<R> ifEmpty, Function3<R, E, R, R> ifNode2, Function5<R, E, R, E, R, R> ifNode3) {
            R leftResult = left.fold(ifEmpty, ifNode2, ifNode3);
            R rightResult = right.fold(ifEmpty, ifNode2, ifNode3);
            return ifNode2.apply(leftResult, elem, rightResult);
        }

        @Override
        protected PersistentTreeSet<E> rightChild() {
            return right;
        }

        @Override
        protected PersistentTreeSet<E> leftChild() {
            return left;
        }

    }

    static class Node3<E> extends PersistentTreeSet<E> {
        final Empty<E> empty;
        final PersistentTreeSet<E> left;
        final E lElem;
        final PersistentTreeSet<E> mid;
        final E rElem;
        final PersistentTreeSet<E> right;

        public Node3(Empty<E> empty, E lElem, E rElem) {
            this.empty = empty;
            this.lElem = lElem;
            this.rElem = rElem;
            this.left = empty;
            this.mid = empty;
            this.right = empty;
        }

        public Node3(Empty<E> empty, PersistentTreeSet<E> left, E lElem, PersistentTreeSet<E> mid, E rElem, PersistentTreeSet<E> right) {
            this.empty = empty;
            this.left = left;
            this.lElem = lElem;
            this.mid = mid;
            this.rElem = rElem;
            this.right = right;
        }

        @Override
        public PersistentTreeSet<E> add(E elem) {
            final int leftCmp = empty.cmp.compare(elem, this.lElem);
            if (leftCmp < 0) {
                return insertLeft(elem);
            } else if (leftCmp == 0) {
                return this;
            } else {
                final int rightCmp = empty.cmp.compare(elem, this.rElem);
                if (rightCmp < 0) {
                    return insertMid(elem);
                } else if (rightCmp > 0) {
                    return insertRight(elem);
                } else {
                    return this;
                }
            }
        }

        private PersistentTreeSet<E> insertRight(E elem) {
            if (right instanceof Empty) {
                return new Node2<>(empty, rElem, new Node2<>(empty, lElem), new Node2<>(empty, elem));
            }

            PersistentTreeSet<E> newRight = right.add(elem);
            if (right == newRight) {
                return this;
            }

            if (right instanceof Node3 && newRight instanceof Node2) {
                return new Node2<>(empty, rElem, new Node2<>(empty, lElem, left, mid), newRight);
            }

            return new Node3<>(empty, left, lElem, mid, rElem, newRight);
        }

        private PersistentTreeSet<E> insertMid(E elem) {
            PersistentTreeSet<E> newMid = mid.add(elem);
            if (mid == newMid) {
                return this;
            }

            if (mid instanceof Node3 && newMid instanceof Node2 || mid instanceof Empty) {
                Node2<E> m = (Node2<E>) newMid;
                return new Node2<>(empty, m.elem, new Node2<>(empty, lElem, left, m.left), new Node2<>(empty, rElem, m.right, right));
            }

            return new Node3<>(empty, left, lElem, newMid, rElem, right);
        }

        private PersistentTreeSet<E> insertLeft(E elem) {
            if (left instanceof Empty) {
                return new Node2<>(empty, lElem, new Node2<>(empty, elem), new Node2<>(empty, rElem));
            }

            PersistentTreeSet<E> newLeft = left.add(elem);
            if (left == newLeft) {
                return this;
            }

            if (left instanceof Node3 && newLeft instanceof Node2) {
                return new Node2<>(empty, lElem, newLeft, new Node2<>(empty, rElem, mid, right));
            }

            return new Node3<>(empty, newLeft, lElem, mid, rElem, right);
        }

        @Override
        public boolean isEmpty() {
            return false;
        }

        @Override
        public boolean contains(E elem) {
            final int leftCmp = empty.cmp.compare(elem, this.lElem);
            if (leftCmp < 0) {
                return left.contains(elem);
            } else if (leftCmp == 0) {
                return true;
            } else {
                final int rightCmp = empty.cmp.compare(elem, this.rElem);
                if (rightCmp < 0) {
                    return mid.contains(elem);
                } else if (rightCmp > 0) {
                    return right.contains(elem);
                } else {
                    return true;
                }
            }
        }

        @Override
        protected RemoveMin<E> removeMin() {
            if (left instanceof Empty) {
                return new RemoveMin<>(lElem, new Node2<>(empty, rElem));
            }

            RemoveMin<E> removeMin = left.removeMin();
            removeMin.result = balanceL(removeMin.result);
            return removeMin;
        }

        @Override
        public PersistentTreeSet<E> remove(E elem) {
            final int leftCmp = empty.cmp.compare(elem, this.lElem);
            if (leftCmp < 0) {
                return balanceL(left.remove(elem));
            } else if (leftCmp == 0) {
                if (right instanceof Empty) {
                    return new Node2<>(empty, rElem);
                } else {
                    RemoveMin<E> removeMin = mid.removeMin();
                    return balanceM(removeMin.result, removeMin.min);
                }
            } else {
                final int rightCmp = empty.cmp.compare(elem, this.rElem);
                if (rightCmp < 0) {
                    PersistentTreeSet<E> newMid = mid.remove(elem);
                    return balanceM(newMid, lElem);
                } else if (rightCmp > 0) {
                    PersistentTreeSet<E> newRight = right.remove(elem);
                    return balanceR(newRight, rElem);
                } else {
                    if (right instanceof Empty) {
                        return new Node2<>(empty, lElem);
                    } else {
                        RemoveMin<E> removeMin = right.removeMin();
                        return balanceR(removeMin.result, removeMin.min);
                    }
                }
            }
        }

        private PersistentTreeSet<E> balanceL(PersistentTreeSet<E> newLeft) {
            if (newLeft == left) {
                return this;
            }
            if (newLeft instanceof Empty || left instanceof Node2 && newLeft instanceof Node3) {
                if (mid instanceof Node2) {
                    Node2<E> n = (Node2<E>) mid;
                    return new Node2<>(empty,
                            rElem,
                            new Node3<>(empty, newLeft, lElem, n.left, n.elem, n.right),
                            right);
                } else {
                    Node3<E> n = (Node3<E>) mid;
                    return new Node3<>(empty,
                            new Node2<>(empty, lElem, newLeft, n.left),
                            n.lElem,
                            new Node2<>(empty, n.rElem, n.mid, n.right),
                            rElem,
                            right);
                }
            } else {
                return new Node3<>(empty, newLeft, lElem, mid, rElem, right);
            }
        }

        private PersistentTreeSet<E> balanceR(PersistentTreeSet<E> newRight, E min) {
            if (newRight == right) {
                return this;
            }
            if (newRight instanceof Empty || right instanceof Node2 && newRight instanceof Node3) {
                if (mid instanceof Node2) {
                    Node2<E> n = (Node2<E>) mid;
                    return new Node2<>(empty,
                            lElem,
                            left,
                            new Node3<>(empty, n.left, n.elem, n.right, min, newRight));
                } else {
                    Node3<E> n = (Node3<E>) mid;
                    return new Node3<>(empty,
                            left,
                            lElem,
                            new Node2<>(empty, n.lElem, n.left, n.mid),
                            n.rElem,
                            new Node2<>(empty, min, n.right, newRight));
                }
            } else {
                return new Node3<>(empty, left, lElem, mid, min, newRight);
            }
        }

        private PersistentTreeSet<E> balanceM(PersistentTreeSet<E> newMid, E min) {
            if (newMid == mid) {
                return this;
            }
            if (newMid instanceof Empty || mid instanceof Node2 && newMid instanceof Node3) {
                if (left instanceof Node2) {
                    Node2<E> n = (Node2<E>) left;
                    return new Node2<>(empty, rElem,
                            new Node3<>(empty, n.left, n.elem, n.right, min, newMid),
                            right);
                } else {
                    Node3<E> n = (Node3<E>) left;
                    return new Node3<>(empty,
                            new Node2<>(empty, n.lElem, n.left, n.mid),
                            n.rElem,
                            new Node2<>(empty, min, n.right, newMid),
                            rElem,
                            right);
                }
            } else {
                return new Node3<>(empty, left, min, newMid, rElem, right);
            }
        }

        @Override
        public <R> R fold(Supplier<R> ifEmpty, Function3<R, E, R, R> ifNode2, Function5<R, E, R, E, R, R> ifNode3) {
            R leftResult = left.fold(ifEmpty, ifNode2, ifNode3);
            R midResult = mid.fold(ifEmpty, ifNode2, ifNode3);
            R rightResult = right.fold(ifEmpty, ifNode2, ifNode3);
            return ifNode3.apply(leftResult, lElem, midResult, rElem, rightResult);
        }

        @Override
        protected PersistentTreeSet<E> rightChild() {
            return right;
        }

        @Override
        protected PersistentTreeSet<E> leftChild() {
            return left;
        }

    }

    private static class RemoveMin<E> {
        E min;
        PersistentTreeSet<E> result;

        public RemoveMin(E min, PersistentTreeSet<E> result) {
            this.min = min;
            this.result = result;
        }
    }

    static abstract class AbstractTreeIterator<E> {
        final Deque<PersistentTreeSet<E>> stack;
        int currentValue = -1;
        PersistentTreeSet<E> currentNode;

        public AbstractTreeIterator(PersistentTreeSet<E> root) {
            this.stack = new ArrayDeque<>(/* root.size() >> 2 + 1 */);
            this.currentNode = root;
        }

        public boolean advance() {
            if (currentValue == -1) {
                currentNode = recurseIntoLeftmostLeaf(currentNode, stack);
                currentValue = 0;
                return true;
            }

            if (currentNode instanceof Node3 && currentValue == 0) {
                Node3<E> node3 = (Node3<E>) currentNode;
                if (node3.mid.isEmpty()) {
                    currentValue = 1;
                } else {
                    stack.push(node3);
                    currentNode = recurseIntoLeftmostLeaf(node3.mid, stack);
                }
                return true;
            }

            PersistentTreeSet<E> rightChild = currentNode.rightChild();
            if (!rightChild.isEmpty()) {
                stack.push(currentNode);
                currentNode = recurseIntoLeftmostLeaf(rightChild, stack);
                currentValue = 0;
                return true;
            }

            while (true) {
                if (stack.isEmpty()) {
                    return false;
                }
                if (isRightChild(stack.peek(), currentNode)) {
                    currentNode = stack.pop();
                } else {
                    break;
                }
            }

            if (isLeftChild(stack.peek(), currentNode)) {
                currentValue = 0;
            } else {
                currentValue = 1;
            }
            currentNode = stack.pop();
            return true;
        }

        private PersistentTreeSet<E> recurseIntoLeftmostLeaf(PersistentTreeSet<E> node, Deque<PersistentTreeSet<E>> stack) {
            if (node instanceof Node2) {
                Node2<E> node2 = (Node2<E>) node;
                if (node2.left.isEmpty()) {
                    return node2;
                } else {
                    stack.push(node2);
                    return recurseIntoLeftmostLeaf(node2.left, stack);
                }
            } else {
                Node3<E> node3 = (Node3<E>) node;
                if (node3.left.isEmpty()) {
                    return node3;
                } else {
                    stack.push(node3);
                    return recurseIntoLeftmostLeaf(node3.left, stack);
                }
            }
        }

        private boolean isRightChild(PersistentTreeSet<E> parent, PersistentTreeSet<E> currentNode) {
            if (parent instanceof Node2) {
                return ((Node2<E>) parent).right == currentNode;
            } else {
                return ((Node3<E>) parent).right == currentNode;
            }
        }

        private boolean isLeftChild(PersistentTreeSet<E> parent, PersistentTreeSet<E> currentNode) {
            if (parent instanceof Node2) {
                return ((Node2<E>) parent).left == currentNode;
            } else {
                return ((Node3<E>) parent).left == currentNode;
            }
        }

        public E get() {
            if (currentNode instanceof Node2) {
                return ((Node2<E>) currentNode).elem;
            } else {
                Node3<E> node3 = (Node3<E>) currentNode;
                if (currentValue == 0) {
                    return node3.lElem;
                } else {
                    return node3.rElem;
                }
            }
        }

    }

    static class TreeIterator<E> extends AbstractTreeIterator<E> implements Iterator<E> {
        boolean hasNext;

        public TreeIterator(PersistentTreeSet<E> root) {
            super(root);
            hasNext = advance();
        }

        @Override
        public boolean hasNext() {
            return hasNext;
        }

        @Override
        public E next() {
            E toReturn = get();
            hasNext = advance();
            return toReturn;
        }
    }

    static class TreeSpliterator<E> extends AbstractTreeIterator<E> implements Spliterator<E> {
        final PersistentTreeSet<E> fence;

        public TreeSpliterator(PersistentTreeSet<E> root) {
            this(root, null);
        }

        public TreeSpliterator(PersistentTreeSet<E> root, PersistentTreeSet<E> fence) {
            super(root);
            this.fence = fence;
        }

        @Override
        public boolean tryAdvance(Consumer<? super E> action) {
            if (currentValue != -1 && currentNode == fence) {
                return false;
            }
            if (advance()) {
                action.accept(get());
                return true;
            } else {
                return false;
            }
        }

        @Override
        public Spliterator<E> trySplit() {
            if (currentValue == -1) {
                currentValue = 1;
                return currentNode.leftChild().spliterator();
            }
//
//            while (true) {
//                if (stack.isEmpty()) {
//                    return false;
//                }
//                if (isRightChild(stack.peek(), currentNode)) {
//                    currentNode = stack.pop();
//                } else {
//                    break;
//                }
//            }
//
//            if (isLeftChild(stack.peek(), currentNode)) {
//                currentValue = 0;
//            } else {
//                currentValue = 1;
//            }
//            currentNode = stack.pop();

            return null;
        }

        @Override
        public long estimateSize() {
            return 0;
        }

        @Override
        public int characteristics() {
            return 0;
        }
    }
}
