package org.bitbucket.pd4j;

public interface PersistentSortedSet<E, Impl extends PersistentSortedSet<E, Impl>> {

    Impl add(E elem);

    Impl remove(E elem);

    boolean contains(E elem);

    boolean isEmpty();

    int size();
}
