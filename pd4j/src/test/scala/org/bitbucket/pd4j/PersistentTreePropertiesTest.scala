
package org.bitbucket.pd4j

import java.util.Comparator
import java.util.function.{Consumer, Supplier}

import org.bitbucket.pd4j.util.Function3
import org.bitbucket.pd4j.util.Function5
import org.junit.Test
import org.scalacheck.{Arbitrary, Gen}
import org.scalacheck.Prop.BooleanOperators
import org.scalatest.junit.JUnitSuite
import org.scalatest.prop.Checkers
import scala.collection.GenTraversableOnce
import scala.collection.JavaConverters._

class PersistentTreePropertiesTest extends JUnitSuite with Checkers {

  implicit override val generatorDrivenConfig = PropertyCheckConfig(minSuccessful = 500, maxDiscarded = 10000)

  @Test
  def spliterator(): Unit = {
    val s = fromTraversable(Iterator.range(0, 100).map(Predef.int2Integer))
    val sp0 = s.spliterator()
    val sp1 = sp0.trySplit()
//    val sp2 = sp0.trySplit()
//    val sp3 = sp1.trySplit()

    val printer = new Consumer[Integer] {
      def accept(t: Integer): Unit = println(t)
    }

    while (sp0.tryAdvance(printer)) {}
    while (sp1.tryAdvance(printer)) {}
//    while (sp2.tryAdvance(printer)) {}
//    while (sp3.tryAdvance(printer)) {}
  }

  @Test
  def `removal overrides addition`() = check { (l: List[Integer], a: Integer) =>
    val s = fromTraversable(l)
    s.add(a).remove(a) == s.remove(a)
  }

  @Test
  def `sets built from the same elements are equal`() = check { l: List[Integer] =>
    val s1 = fromTraversable(l)
    val s2 = fromTraversable(l.reverse)
    s1.hashCode() == s2.hashCode() && s1 == s2
  }

  @Test
  def `addition overrides removal`() = check { (s: PersistentTreeSet[Integer], a: Integer) =>
    s.remove(a).add(a) == s.add(a)
  }

  @Test
  def `element is contained after insertion`() = check { (s: PersistentTreeSet[Integer], a: Integer) =>
    s.add(a).contains(a)
  }

  @Test
  def `addition is idempotent`() = check { (s: PersistentTreeSet[Integer], a: Integer) =>
    s.contains(a) ==> (s.add(a) eq s)
  }

  @Test
  def `size is increased after insertion`() = check { (s: PersistentTreeSet[Integer], a: Integer) =>
    !s.contains(a) ==> (s.add(a).size() == s.size() + 1)
  }

  @Test
  def `element is not contained after removal`() = check { (s: PersistentTreeSet[Integer], a: Integer) =>
    s.contains(a) ==> !s.remove(a).contains(a)
  }

  @Test
  def `removal is idempotent`() = check { (s: PersistentTreeSet[Integer], a: Integer) =>
    !s.contains(a) ==> (s.remove(a) eq s)
  }

  @Test
  def `removal decreases size`() = check { (l: List[Integer], a: Integer) =>
    val s = fromTraversable(l)
    s.contains(a) ==> (s.remove(a).size() == s.size() - 1 || s.isEmpty)
  }

  @Test
  def `iteration in order`() = check { s: PersistentTreeSet[Integer] =>
    val l = s.iterator().asScala.to[List]
    l == l.sorted
  }

  @Test
  def `the tree is balanced`() = check { buildInstructions: List[SetBuilder[Integer]] =>
    val checkDepths = fold[Integer, (Int, Boolean)](
      () => (0, true),
      (a, _, c) => (Math.max(a._1, c._1) + 1, a._1 == c._1),
      (a, _, c, _, e) => (Math.max(a._1, Math.max(c._1, e._1)) + 1, a._1 == c._1 && a._1 == e._1)) _

    val s = buildInstructions.foldLeft(PersistentTreeSet.empty[Integer]())((s, f) => f(s))

    checkDepths(s)._2
  }

  type SetBuilder[A] = PersistentTreeSet[A] => PersistentTreeSet[A]

  @Test
  def `the tree is sorted`() = check { buildInstructions: List[SetBuilder[Integer]] =>
    val checkSortednes = fold[Integer, (Option[(Integer, Integer)], Boolean)](
      ifEmpty = () => (None, true),
      ifNode2 = (left, elem, right) => {
        val leftIsSmaller = left._1.map { case (_, max) => max < elem } getOrElse(true)
        val rightIsGreater = right._1.map { case (min, _) => min > elem } getOrElse(true)

        val leftMin = left._1.map { _._1 } getOrElse(elem)
        val rightMax = right._1.map { _._2 } getOrElse(elem)

        (Some((leftMin, rightMax)), leftIsSmaller && rightIsGreater)
      },
      ifNode3 = (left, lElem, mid, rElem, right) => {
        val leftIsSmaller = left._1.map { case (_, max) => max < lElem } getOrElse(true)
        val midIsInBetween = mid._1.map { case (min, max) => min > lElem && max < rElem } getOrElse(true)
        val rightIsGreater = right._1.map { case (min, _) => min > rElem } getOrElse(true)

        val leftMin = left._1.map { _._1 } getOrElse(lElem)
        val rightMax = right._1.map { _._2 } getOrElse(rElem)

        (Some((leftMin, rightMax)), leftIsSmaller && midIsInBetween && rightIsGreater && lElem < rElem)
      }) _

    val s = buildInstructions.foldLeft(PersistentTreeSet.empty[Integer]())((s, f) => f(s))

    checkSortednes(s)._2
  }

  def fold[A, B](ifEmpty: () => B, ifNode2: (B, A, B) => B, ifNode3: (B, A, B, A, B) => B)(s: PersistentTreeSet[A]): B = {
    s.fold(
      new Supplier[B] {
        override def get(): B = ifEmpty()
      },
      new Function3[B, A, B, B] {
        override def apply(a: B, b: A, c: B): B = ifNode2(a, b, c)
      },
      new Function5[B, A, B, A, B, B] {
        override def apply(a: B, b: A, c: B, d: A, e: B): B = ifNode3(a, b, c, d, e)
      }
    )
  }

  implicit val arbInteger = Arbitrary(Arbitrary.arbitrary[Int].map(Integer.valueOf))
  implicit val intComparator = Comparator.naturalOrder[Integer]()
  implicit val intOrdering = Ordering.comparatorToOrdering[Integer]

  implicit def treeSetGenerator[A](implicit c: Comparator[A], arb: Arbitrary[A]): Arbitrary[PersistentTreeSet[A]] = Arbitrary {
    Gen.containerOf[Set, A](Arbitrary.arbitrary[A]).map(fromTraversable(_))
  }

  implicit def builderGenerator[A](implicit c: Comparator[A], arb: Arbitrary[A]): Arbitrary[List[SetBuilder[A]]] = Arbitrary {
    def add(v: A): SetBuilder[A] = _.add(v)
    def remove(v: A): SetBuilder[A] = _.remove(v)

    for {
      values <- Gen.containerOf[List, A](Arbitrary.arbitrary[A])
      builders <- Gen.sequence[List[SetBuilder[A]], SetBuilder[A]](values.map(v => Gen.oneOf(add(v), remove(v))))
    } yield builders
  }

  def fromTraversable[A](l: GenTraversableOnce[A])(implicit c: Comparator[A]): PersistentTreeSet[A] = {
    val empty = PersistentTreeSet.empty[A](c)
    l.foldLeft(empty)((s, i) => s.add(i))
  }
}