package org.bitbucket.pd4j;

import org.bitbucket.pd4j.PersistentTreeSet;
import org.junit.Test;

import java.lang.Integer;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PersistentTreeSetIteratorTest {

    @Test
    public void test() {
        PersistentTreeSet<Integer> s = PersistentTreeSet.<Integer>empty();
        for (int i = 0; i < 100; i++) {
            s = s.add(i);
        }

        PersistentTreeSet.TreeIterator<Integer> iterator = new PersistentTreeSet.TreeIterator<>(s);

        for (int i = 0; i < 100; i++) {
            assertTrue("" + i, iterator.hasNext());
            assertEquals(i, iterator.next().intValue());
        }

        assertFalse(iterator.hasNext());
    }


}
